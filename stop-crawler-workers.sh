#!/bin/bash
ps -ef | grep php | grep crawler | grep instance | grep -v grep | awk '{print $2}' | xargs kill
sleep 1
echo "Workers stopped. Survived workers:"
ps -ef | grep php | grep crawler | grep instance | grep -v grep
