<?php

/******************************************************************************
 * Program: Image Crawler
 * Author:  Domagoj Marsic <domagoj.marsic@gmail.com>
 * Version: 1.1
 * Created: Jan 28 2014
 * Updated: Feb 15 2014
 *
 * Searches a table with product links for the unprocessed URLs. For such URL
 * it finds the best image (by comparing URL tokens to IMG ALT content) and
 * updates image data in the database.
 *
 * Recommended usage, in a loop:
 *   $product = new product({'crawler'|'sitemap'});
 *   $product->set_db($dbconn, $dbuser, $dbpass);
 *   $product->find_next_url('file', $URLSOURCE); // or find_next_url_phpcrawl
 *   $product->set_url_tokens();
 *   $product->get_product_image
 *   $product->store_image_data
*****************************************************************************/

require_once('config.inc.php');
require_once('common.inc.php');
require_once('product.class.php');


$opts = getopt("hd:o:1");
if (isset($opts['h'])) {
    help();
    exit(1);
}
$domain_filter = null;
if (isset($opts['d'])) {
    $domain_filter = $opts['d'];
}
$outfile = null;
if (isset($opts['o'])) {
    $outfile = $opts['o'];
}

// Process products in an infinite loop. Ends when there are no more
// URLs that don't have 'date_processed' set.
while (1) {

    $product = new Product();
    $product->debug(DEBUG_LEVEL);
    $product->set_db(DBCONN, DBUSER, DBPASS);
    $product->set_domain_filter($domain_filter);
    $product->set_image_basedir(IMAGE_BASE_PATH);
    
    // Find next available product (from database)
    if (! $product->find_next_url()) {
        print "No more products available.\n";
        break;
    }
    
    // URL tokens are used as a means of checking the correct product image.
    if (! $product->set_url_tokens()) {
        print "URL not set.\n";
        continue;
    }

    // From the page, get image and save image data to the database.
    if (! $content = $product->get_content()) {
        print "Failed to get URL.\n";
        continue;
    }
    if (isset($outfile)) {
        $product->save_content($outfile);
    }
    
    $product->get_page_title();
    $product->find_price();
    $product->find_image();
    $product->save_image_locally();
    if (! $product->store_image_and_price_data()) {
        print "Failed to save data to database.\n";
    }
    
    // Exit if "-1" command-line argument was passed. This is to ensure
    // we can run tests with only one page.
    if (isset($opts['1'])) {
        print "Exiting by request.\n";
        break;
    }
}

function help() {

    print <<<HELP
Crawler

Usage:
    php crawler.php [options]
    
Options:
    h                   Print this help.
    d [filter]          Specify the domain filter, e.g. 'elganso.com'
    o [filename]        Save page to the filename.
    1                   Process 1 page and exit.
    
HELP;
}

?>