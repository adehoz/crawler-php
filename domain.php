<?php

/******************************************************************************
 * Class Domain
 * Author:  Domagoj Marsic <domagoj.marsic@gmail.com>
 * Version: 1.0
 * Created: Feb 09 2014
 * Updated: ---
 *
 * Provides all available links for given domains. Domains can be read either
 * from a file or database table, and they are searched for:
 *   o  robots.txt
 *   o  sitemap.xml files
 *   o  all links by crawling the site
 * Domain information is kept in the database table, 'domains'.
 * Product information obtained from sitemaps is kept in the database table,
 * 'pages'. Product information obtained by php-crawler (external
 * program) is kept in database table, 'phpcrawler_links'.
 *
 * Recommended usage, in a loop:
 *   $domain = new Domain();
 *   $domain->set_db_properties($dbconn, $dbuser, $dbpass);
 *   $domain->find_url_from('file', $URLSOURCE);
 *   $domain->find_products();
 *****************************************************************************/

require_once('config.inc.php');
require_once('common.inc.php');

include("PHPCrawl_CURRENT/libs/PHPCrawler.class.php"); 

class MyCrawler extends PHPCrawler
{
    private $_mod_count = 0;
    private $_mod_limit = 100;
    
    private $_dbh = null;
    private $_common = null;
    
    private $_domain_url = '';
    
    private $_products = array();
    
    // Set database handler.
    public function set_dbh($dbh) {
        $this->_dbh = $dbh;
    }
    
    // Set common routines handler.
    public function set_common($common) {
        $this->_common = $common;
    }
    
    // Set domain url (source of all pages)
    public function set_domain_url($domain_url) {
        $this->_domain_url = $domain_url;
    }
    
    // Function extended from PHPCrawler.
    function handleDocumentInfo(PHPCrawlerDocumentInfo $PageInfo) {
    
        $this->_mod_count ++;

        if ($PageInfo->content_type == 'text/html') {
            echo $PageInfo->url . "\n";
            $this->_products[] = $PageInfo->url;
        }

        // Save every mod_limit entries to database and empty products repo.
        if ($this->_mod_count % $this->_mod_limit == 0) {
            $this->save_products();
            $this->_products = array();
        }
    }
    
    
    // Save pages to the database.
    public function save_products() {
    
        // Skip if no new products.
        if (count($this->_products) == 0) {
            return;
        }
        
        // Prepare insert statement.
        $ins_values = array();
        foreach ($this->_products as $product) {
            $safe_product_url =
                $this->_dbh->handler->quote($product);
            $sitemap_id = 
                "(SELECT id FROM sitemaps WHERE sitemap_url = '"
                . $this->_domain_url . "')";
            $ins_values[] = "(NULL, " . $safe_product_url . ", "
                . "MD5(" . $safe_product_url . "), " . $sitemap_id . ")";
        }
        $this->_common->writeit(2, "PRODUCTS: Commiting " . count($ins_values)
            . " entries");
        
        try {
        
            $ins = "INSERT IGNORE INTO pages "
                . "(id, page_url, page_url_md5, sitemap_id) VALUES "
                . join(",", $ins_values);
            $q = $this->_dbh->handler->prepare($ins);
            $q->execute();
        }
        
        catch (PDOException $e) {
            fprint(STDERR, "ERROR: Failed to save domain data --> "
                . $e->getMessage() . " (" . $e->getCode() . ")\n\n"
            );
        }
    }
}

class Domain extends PHPCrawler
{

    // Properties.
    private $_source_type = '';         // 'file' or 'table'
    private $_source_name = '';         // source filename or table with URLs
    private $_original_url = '';        // Whatever user put in the source
    private $_domain_url = '';          // Domain url (e.g. www.amazon.com)
    private $_domain_path = '';         // User-specified part of the path
    private $_domain_id = 0;            // Retrieved on insert.
    private $_method = '';              // How the links are obtained:
                                        // sitemap.xml, robots.txt, crawl
    private $_user_def_sitemap = 0;     // flag whether user specified a s'map.
    private $_force_crawl = 0;          // flag to force crawl method
    private $_source_urls = array();    // Contains URLs of the method files.
    private $_sitemaps = array();       // Contains new sitemaps. MEM!
    private $_products = array();       // Contains new products. MEM!
    
    private $_mysql_insert_chunk = 100; // split multi-value insert chunk size

    private $_domain_status = array(
        'STARTED'    => 0,
        'COMPLETED'  => 1
    );
    
    private $_common = null;             // common class instance
    private $_dbh = null;                // database handler
    
    // Constructor. Accept domain url (e.g. www.amazon.com)
    public function __construct() {
        $this->_common = new Common();
    }
    
    // Return HTTP response code for a given URL.
    private function _check_http_response($url) {

        $default_resp = -1;
    
        $headers = get_headers($url, 0);
        if ($headers === FALSE) {
            return $default_resp;
        }
        $response_code = -1;
        
        foreach ($headers as $header) {
        
            if (preg_match("/HTTP\/1.\d (\d+)/", $header, $matches)) {
            
                $response_code = $matches[1];
            }
        }
        
        return $response_code;
    }
    
    // This is a URL syntax validation function for domains.
    // Domain URL: [protocol://]host[:port]
    // Domain path: the path
    // Returns array(domain_url, domain_path)
    private function _check_url($url) {
    
        $components = parse_url($url);
        $domain_url = '';
        $domain_path = '';
        
        // Build domain url
        if (isset($components['scheme'])) {
            $domain_url = $components['scheme'] . '://';
        }
        
        if (isset($components['host'])) {
            $domain_url .= $components['host'];
        }
        else {
            $domain_url .= 'localhost';
        }
        
        if (isset($components['port'])) {
            $domain_url .= ':' . $components['port'];
        }
        
        if (isset($components['path'])) {
            $domain_path = $components['path'];
        }

        printf("Domain name validation --> %s\n", $domain_url);
        return array($domain_url, $domain_path);
    }
    
    // Read domain status from database.
    private function _url_status() {
    
        $status = null;
        try {
            $q = <<<QUERY
SELECT status FROM domains WHERE domain_url = :domain_url
QUERY;
            $sth = $this->_dbh->handler->prepare($q);
            $sth->bindParam(":domain_url", $this->_original_url);
            $sth->execute();
            $status = $sth->fetchColumn();
            if ($status == '') {
                $status = null;
            }
        }
        
        catch (Exception $e) {
            die("ERROR: Could not get status for domain " . $this->domain_url
                . ", message" . $e->getMessage() . " [" . $e->getCode()
                . "]");
        }
        
        return $status;
    }
    
    // Check methods function checks how product links can be obtained:
    // 1. sitemap.xml
    // 2. robots.txt
    // 3. "manually" with a crawler.
    // Saves an array containing a URL => {sitemap, robots} or "crawl" => 
    // "crawl"
    // Requested 31.08.2014. Robots and sitemaps work with the base domain,
    // but the crawler works with the given url.
    private function _check_methods() {

    
        // user-defined sitemap
        if ($this->_user_def_sitemap == 1) {
            $sitemap_urls = $this->_sitemaps;
        }
    
        // sitemap.xml
        else {
            $sitemap_urls = array(
                $this->_domain_url . "/sitemap.xml",
                $this->_domain_url . "/sitemap.axd"
            );
        }
        
        
        // If force crawl flag is set, return crawl.
        if ($this->_force_crawl == 1) {
            $this->_common->writeit(0, "Force crawl.");
            $this->_source_urls[] = array(
                "type" => "crawl",
                "url"  => $this->_original_url
            );
            return "crawl";
        }
        
        // Check whether a sitemap is available
        foreach ($sitemap_urls as $sitemap_url) {
            $this->_common->writeit(2, "Checking sitemap: $sitemap_url");
            $status = $this->_check_http_response($sitemap_url);
            if ($status == 200) {
                $this->_source_urls[] = array(
                    "type" => "sitemap",
                    "url"  => $sitemap_url
                );
                $this->_common->writeit(2, "method is sitemap.");
                return "sitemap";
            }
        }
        
        // robots.txt
        $robots_url = $this->_domain_url . "/robots.txt";
        $status = $this->_check_http_response($robots_url);
        if ($status == 200) {
            $this->_source_urls[] = array(
                "type" => "robots",
                "url"  => $robots_url
            );
            $this->_common->writeit(2, "method is robots.");
            return "robots";
        }
        
        $this->_common->writeit(2, "method is crawl.");
        $this->_source_urls[] = array(
            "type" => "crawl",
            "url"  => $this->_original_url
        );
        return "crawl";
    }
    
    // Get the first URL from the file. Remove the line from file so
    // the further crawling attempts will not process the same URL.
    private function _pop_url_from_file($filename) {
    
        $domain = '';
        
        $urls = @file($filename, FILE_IGNORE_NEW_LINES);
        if ($urls === FALSE) {
            fwrite(STDERR, "WARN: File $urlfile not read!\n");
        }
        else {
            if (isset($urls[0])) {
                $domain = array_shift($urls);
                $rv = file_put_contents($filename, join("\n", $urls));
                if ($rv === FALSE) {
                    fwrite(STDERR,
                        "WARN: Failed to write URL list to $urlfile.\n");
                }
            }
        }
        
        return $domain;
    }
    
    // Set debug level, -1 for PRODUCTION!
    public function debug($level) {
        $this->_common->set_debug($level);
    }
    
    // Sets database parameters.
    public function set_db_properties($dbconn, $dbuser, $dbpass) {
        $this->_dbh = new Database($dbconn, $dbuser, $dbpass);
    }
    
    // Set user-defined sitemap. Only this sitemap will be processed.
    public function set_sitemap($sitemap) {
        if (isset($sitemap)) {
            $this->_user_def_sitemap = 1;
            $this->_sitemaps[] = $sitemap;
        }
    }
    
    // Sets a flag to force crawl. Useful when a sitemap doesn't contain pages
    // but just categories and similar.
    public function force_crawl($f_force_crawl) {
        $this->_force_crawl = $f_force_crawl;
    }
    
    // Sets the input for URLs and gets the first available url.
    public function find_url_from($srctype, $name) {
    
        $this->_source_type = $srctype;
        $this->_source_name = $name;
        
        $domain_info = array();
        $domain = '';
        
        
        if ($srctype == 'user-defined') {
            if (! isset($this->_sitemaps[0])) {
                $this->_common->writeit(1, "Sitemap not set.");
                return 2;
            }
            
            $domain = $this->_sitemaps[0];
            $domain_info = $this->_check_url($domain);
            if (null !== $this->_url_status()) {
                return 1;
            }
        }
        
        elseif ($srctype == 'file') {
            $domain = trim($this->_pop_url_from_file($name));
            if ($domain == '') {
                $this->_common->writeit(1, "Empty string read from source.");
                return 2;       // error in input, will exit program.
            }

            $domain_info = $this->_check_url($domain);
            if (null !== $this->_url_status()) {
                return 1;
            }
        }

        elseif ($srctype == 'table') {
            $domain = $this->_find_available_domain();
            if (! isset($domain)) {
                $this->_common->writeit(1, "No domains available.");
                return 2;
            }
            $domain_info = $this->_check_url($domain);
        }
        else {
            fwrite(STDERR, "ERROR: Illegal specification of source type.\n");
            exit;
        }
        
        $this->_original_url = $domain;
        $this->_domain_url = $domain_info[0];
        $this->_domain_path = $domain_info[1];
        
        
        print "Domain (url, path): [" . $this->_domain_url . "]["
            . $this->_domain_path. "]\n";

        $this->_method = $this->_check_methods();
        $this->save_domain($this->_domain_status['STARTED']);
        
        return 0;
    }
    
    // Search domains table for a domain that has status either null
    // or status = 0. If status = 0, then check if the pid exists on the
    // server. Update the domain found with own pid.
    // NOTE: There's a slight possibility of race condition when multiple
    // workers run between SELECT and UPDATE.
    private function _find_available_domain() {
    
        $pid = getmypid();
    
        $q = "SELECT * FROM domains WHERE status IS NULL OR status = 0";

        try {
        
            $sth_q = $this->_dbh->handler->prepare($q);
            $sth_q->execute();

            $rows = $sth_q->fetchAll(PDO::FETCH_ASSOC);
            if ($rows === FALSE || empty($rows)) {
                return NULL;
            }
        }
        
        catch (PDOException $e) {
            fwrite(STDERR, "ERROR: Query: $q. " . $e->getMessage() . " ["
                . $e->getCode() . "]\n");
            return NULL;
        }
        
        $domain_row = null;
        foreach ($rows as $row) {
        
            $domain_row = $row;
            
            // If status is null, that's our row.
            if (! isset($row['status'])) {
                $this->_common->writeit(2, "Row selected <-- status = null.");
                break;
            }
        
            // if domain processing is in progress...
            elseif ($row['status'] == 0) {
            
                // ...and we don't have info on process id, that's it.
                if (! isset($row['process_id'])) {
                    $this->_common->writeit(2, "Row selected <-- "
                        . "status = 0, PID is not set.");
                    break;
                }
                
                // ...or the process id does not exist, that's it.
                elseif (isset($row['process_id'])
                && ! is_dir("/proc/" . $row['process_id'])) {
                    $this->_common->writeit(2, "Row selected <-- "
                        . "status = 0, PID not found.");
                    break;
                }
                
                // Finally, processing is in progress and we can see the
                // active process. $domain_row needs to be explicitly
                // nullified.
                else {
                    $domain_row = null;
                }
            }
        }
        
        // If domain row is still null, we didn't find a domain to process.
        if (! isset($domain_row)) {
            $this->_common->writeit(2, "No suitable rows found.");
            return NULL;
        }

        $upd = <<<QUERY
UPDATE domains
SET process_id = :process_id
WHERE id = :id
QUERY;
        
        try {
            $sth_u = $this->_dbh->handler->prepare($q);
            $sth_u->bindParam(":process_id", $pid);
            $sth_u->bindParam(":id", $domain_row['id']);
            $sth_u->execute();
        }
        
        catch (PDOException $e) {
            fwrite(STDERR, "ERROR: Failed to update 'domains'. Q: " . $upd
                . ", Message: " . $e->getMessage() . ", [" . $e->getCode()
                . "]\n");
            return NULL;
        }
        
        // Return domain url.
        print "Found this domain for processing.\n";
        print_r($domain_row);
        return $domain_row['domain_url'];
    
    }
    
    // Get domain method.
    public function search_method() {
        return $this->_method;
    }
    
    // Parse robots.txt contents and return found links.
    public function get_robots_links($url) {
        
        $content = file_get_contents($url);
        $content_arr = explode("\n", $content);
        
        foreach ($content_arr as $line) {
        
            // Search for sitemaps.
            if (preg_match("/^Sitemap:\s*(\S+)\s*/i", $line, $matches)) {
                $sitemap = $matches[1];
                $this->_sitemaps[] = $sitemap;
                $this->_source_urls[] = array(
                    "type" => "sitemap",
                    "url"  => $sitemap
                );
                //print "Sitemap found in robots.txt: " . $sitemap . "\n";
            }
        }
    }
    
    
    // Parse sitemap contents. It may contain links but also other sitemaps.
    // This creates a large memory structure so we cannot hold everything in
    // memory (or it would require changing php.ini parameters).
    public function get_sitemap_links($url) {
        
        $content = file_get_contents($url);
        
        if (substr($url, strlen($url) - 3) == '.gz') {
            print "Decoding $url\n";
            $content_dec = gzdecode($content);
            print_r($content_dec);
            if ($content_dec === FALSE || $content_dec == "") {
                $this->_common->writeit(1, "Failed to decode $url");
                if (strtolower(substr($content, 0, 5)) == '<?xml') {
                    // It seems this is just a regular XML, not gzipped one.
                    $this->_common->writeit(1, "Wait, this is just an XML.");
                    $content_dec = $content;
                }
                else {
                    // Could not open this gzip
                    return 0;
                }
            }
            $content = $content_dec;
        }
        
        //print "CONTENT $url\n\n$content\n\n";
        //readline();
        
        try {
            $xml = new SimpleXMLElement($content);
        }
        catch (Exception $e) {
            fwrite(STDERR, "ERROR: Exception on sitemap $url: "
                . $e->getMessage() . " [" . $e->getCode() . "]\n");
            return 0;
        }
        
        // Go through url nodes.
        foreach ($xml->url as $xmlurl) {
            
            // Get locations from sitemap xml document.
            $loc = $xmlurl->loc;
            $this->_products[] = array(
                "source_url" => $url,
                "product_url" => $loc
            );
            //print "Sitemap loc found: $loc\n";
            
        }
        
        // Go through sitemap urls. These will be added to the sitemap list.
        foreach ($xml->sitemap as $xmlsitemap) {
        
            // Get sitemap locations.
            $loc = $xmlsitemap->loc;
            $this->_sitemaps[] = $loc;
            $this->_source_urls[] = array(
                "type" => "sitemap",
                "url"  => $loc
            );
            //print "Sitemapception: $loc\n";
        }
        
        return 1;
    }
    
    private function _ungzip($content) {
        
        $tmpfile = sys_get_temp_dir() . "/" . md5(time());
        $tmpfilegz = $tmpfile . ".gz";
        
        file_put_contents($tmpfilegz, $content);
        system("gunzip $tmpfilegz");
        $this->_common->writeit(2, "gunzipped!");
        
        return file_get_contents($tmpfile);
    }
    
    public function crawl($url) {

        // Calls the external tool to crawl the website. All configuration
        // is configured with the called tool and this method only calls
        // the tool. This includes writing to the database.
    
        print "Crawling: $url\n";
        
        $crawler = new MyCrawler();
        $crawler->set_dbh($this->_dbh);
        $crawler->set_common($this->_common);
        $crawler->set_domain_url($url);
        $crawler->setURL($url);
        $crawler->addContentTypeReceiveRule("#text/html#");
        $crawler->setFollowMode(3);
        $crawler->obeyNoFollowTags(TRUE);
        $crawler->setUrlCacheType(PHPCrawlerUrlCacheTypes::URLCACHE_SQLITE);
        $crawler->go();
        $crawler->save_products();
        print "Done crawling for $url\n";
        return 0;
    }
    
    
    // Given the search method (robots, sitemap, crawl), finds all available
    // product links.
    public function find_products() {
    
        while ($source = array_pop($this->_source_urls)) {
        
            $this->_common->writeit(2, "FIND PRODUCTS: " . $source["url"]);
        
            if ($source["type"] == "robots") {
                $this->_common->writeit(1, 
                    "Searching robots: " . $source["url"]);
                $this->get_robots_links($source["url"]);
                $this->_common->writeit(2, "Found " . count($this->_sitemaps)
                    . " sitemaps.");
                if (count($this->_sitemaps) > 0) {
                    $this->save_products();
                }
                else {
                    $source["type"] = "crawl";
                    $source["url"]  = $this->_original_url;
                }
            }
            
            if ($source["type"] == "sitemap") {
                print "Searching sitemap: " . $source["url"] . "\n";
                $this->_sitemaps[] = $source["url"];
                $rv = $this->get_sitemap_links($source["url"]);

                if ($rv == 1) {
                    $this->save_sitemaps();
                    $this->save_products();
                    $this->clear_sitemaps();
                } 
                else {
                    $source["type"] = "crawl";
                    $source["url"]  = $this->_original_url;
                }
            }
            
            if ($source["type"] == "crawl") {
                print "Starting crawler: " . $source["url"] . "\n";
                $this->_method = "crawl";
                $this->save_crawl_method();
                
                // Save sitemaps -> just to be able to connect pages with 
                // domains.
                $this->_sitemaps = array($this->_original_url);
                $this->save_sitemaps();
                
                $status = $this->crawl($source["url"]);
                
                $this->clear_sitemaps();

                // If crawl failed, don't update domain status
                if ($status == 1) {
                    return 1;
                }
            }
        }
        
        // At the end, update domain status as completed.
        $this->save_domain($this->_domain_status['COMPLETED']);
        return 0;
    }
    
    // Save products to database. Creates a single multiple-values insert
    // as this is the fastest way. All products are in memory and saving a 
    // lot of products can take a while.
    public function save_products() {
    
        // Skip if no new products.
        if (count($this->_products) == 0) {
            return;
        }
        
        // Print and prepare INSERT statement.
        $ins_values = array();
        foreach ($this->_products as $product) {
            $safe_product_url = 
                $this->_dbh->handler->quote($product['product_url']);
            $safe_source_url = 
                $this->_dbh->handler->quote($product['source_url']);
            $ins_values[] = "(NULL, " . $safe_product_url . ", "
                . "MD5(" . $safe_product_url . "), "
                . "(SELECT id FROM sitemaps WHERE sitemap_url = "
                . $safe_source_url . "))";
        }

        try {
            // save in chunks if the size of multi-value insert would be
            // larger than (arbitrarily defined) $this->_mysql_insert_chunk
            
            $counter = 0;
            while ($insert_chunk = array_slice
            ($ins_values, $counter, $this->_mysql_insert_chunk)) {
                
                $insert = "INSERT IGNORE INTO pages "
                    . "(id, page_url, page_url_md5, sitemap_id) "
                    . "VALUES " . join(",", $insert_chunk);
                //print $insert . "\n\n";
                $this->_common->writeit(2, "PRODUCTS: Commiting " 
                    . count($insert_chunk) . " entries");
                $q = $this->_dbh->handler->prepare($insert);
                $q->execute();
            
                $counter += $this->_mysql_insert_chunk;
                if ($counter > count($ins_values)) {
                    $counter = $counter 
                        - $this->_mysql_insert_chunk + count($ins_values);
                    break;
                }
            }
            $this->_common->writeit(1, 
                "PRODUCTS: Commited " . $counter . " entries.");
        }
        
        catch (PDOException $e) {
            print "ERROR: Failed to save domain data (" . $this->_domain_url
                . ", " . $this->_method. "), exception: " . $e->getMessage()
                . " (" . $e->getCode() . ")\n\n";
        }
    
        // Clear up products array to free up memory.
        $this->_products = array();
    }
    
    // Update domains table to reflect crawl method.
    public function save_crawl_method() {
    
        $q = <<<QUERY
UPDATE domains
SET search_method = 2
WHERE id = :domain_id
QUERY;
    
        try {
            $stmt = $this->_dbh->handler->prepare($q);
            $stmt->bindParam(":domain_id", $this->_domain_id);
            $stmt->execute();
        }
        
        catch (PDOException $e) {
            print "ERROR: Failed to update domain data (" . $this->_original_url
                . ", " . $this->_method. "), exception: " . $e->getMessage()
                . " (" . $e->getCode() . ")\n\n";
        }
        
        $this->_common->writeit(1, "Domain information updated: "
            . $this->_original_url);
    }
    
    // Save sitemaps to database. Next search can be faster performed with
    // sitemaps already loaded from our database.
    public function save_sitemaps() {
    
        // Skip if no new sitemaps.
        if (count($this->_sitemaps) == 0) {
            return;
        }
        
        // Iterate through sitemaps, print and prepare INSERT statement.
        $ins_values = array();
        foreach ($this->_sitemaps as $sitemap) {
            //print "SITEMAP:\n\t" . $sitemap . "\n";
            $ins_values[] = "(NULL, '" . $sitemap . "', (SELECT id " 
                . "FROM domains WHERE domain_url = '"
                . $this->_original_url
                . "'))";
        }
        

        try {
            // save in chunks if the size of multi-value insert would be
            // larger than (arbitrarily defined) $this->_mysql_insert_chunk
            
            $counter = 0;
            while ($insert_chunk = array_slice
            ($ins_values, $counter, $this->_mysql_insert_chunk)) {
                $insert = "INSERT IGNORE INTO sitemaps "
                    . "(id, sitemap_url, domain_id) VALUES "
                    . join(",", $insert_chunk);
                //print $insert . "\n\n";
                $this->_common->writeit(1, "SITEMAPS: Commiting " 
                    . count($insert_chunk) . " entries");
                $q = $this->_dbh->handler->prepare($insert);
                $q->execute();
                
                $counter += $this->_mysql_insert_chunk;
                if ($counter > count($ins_values)) {
                    break;
                }
            }
        }
        
        catch (PDOException $e) {
            print "ERROR: Failed to save domain data (" . $this->_original_url
                . ", " . $this->_method. "), exeption: " . $e->getMessage()
                . " (" . $e->getCode() . ")\n\n";
        }
                
    
    }
    
    // Sitemaps should be cleared after they are saved and products referenced
    // by these sitemaps also saved.
    public function clear_sitemaps() {
        $this->_sitemaps = array();
    }
    
    // Save domain information to the database. Needs to be called explicitly
    // because we don't want side effects during test runs.
    public function save_domain($status) {

        // Set numeric value for search method.
        $search_method = -1;
        if ($this->_method == "robots") {
            $search_method = 0;
        }
        elseif ($this->_method == "sitemap") {
            $search_method = 1;
        }
        elseif ($this->_method == "crawl") {
            $search_method = 2;
        }
        
        $pid = getmypid();
        
        try {
        
            // save
            $q = <<<QUERY
INSERT INTO domains (id, domain_url, search_method, status, 
process_id, created)
VALUES (NULL, :domain_url, :search_method, :status, :pid, NOW()) 
ON DUPLICATE KEY
UPDATE domain_url = VALUES(domain_url),
search_method = VALUES(search_method),
status = :status,
process_id = :pid
QUERY;
            $sth = $this->_dbh->handler->prepare($q);
            $sth->bindParam(":domain_url", $this->_original_url);
            $sth->bindParam(":search_method", $search_method);
            $sth->bindParam(":status", $status);
            $sth->bindParam(":pid", $pid);
            $sth->execute();
            
            $this->_domain_id = $this->_dbh->handler->lastInsertId();
        }
        
        catch (PDOException $e) {
            print "ERROR: Failed to save domain data (" . $this->_original_url
                . ", " . $this->_method. "), exeption: " . $e->getMessage()
                . " (" . $e->getCode() . ")\n\n";
        }
        
        $this->_common->writeit(1, "Domain information saved: "
            . $this->_original_url);
    }
}


//----------------------------------------------------------------------------
// M A I N
//----------------------------------------------------------------------------

$mode = 'file';
$urlsource = 'urls.txt';

// Command-line options
$opts = getopt("m:s:c");
if (! isset($opts['m'])) {
    $mode = 'file';
    $urlsource = 'urls.txt';
}
elseif ($opts['m'] == 'database' || $opts['m'] == 'd') {
    $mode = 'table';
    $urlsource = 'domains';
}

$sitemap = null;
if (isset($opts['s'])) {
    $sitemap = $opts['s'];
    $s_tokens = parse_url($sitemap);
    if (! isset($s_tokens['scheme'])) {
        $sitemap = 'http://' . $sitemap;
    }
    $mode = 'user-defined';
}

$f_force_crawl = 0;
if (isset($opts['c'])) {
    $f_force_crawl = 1;
}


// or table: $urlsource = 'domains';

$c = 0;
while(1) {

    $domain = new Domain();
    $domain->debug(DEBUG_LEVEL);
    $domain->set_db_properties(DBCONN, DBUSER, DBPASS);
    $domain->set_sitemap($sitemap);
    $domain->force_crawl($f_force_crawl);
    
    $rv = $domain->find_url_from($mode, $urlsource);
    if ($rv == 1 && $mode != 'user-defined') {
        continue;
    }
    if ($rv == 2) {
        break;
    }
    // or: $domain->find_url_from('table', $urlsource);
    $status = $domain->find_products();
    if ($status > 0) {
        break;
    }
    
    if ($mode == 'user-defined') {
        break;
    }
}

?>