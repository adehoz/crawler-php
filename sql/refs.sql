-- MySQL dump 10.14  Distrib 5.5.36-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: cc
-- ------------------------------------------------------
-- Server version	5.5.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `refs`
--

DROP TABLE IF EXISTS `refs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(50) NOT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `refs_w` (`word`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refs`
--

LOCK TABLES `refs` WRITE;
/*!40000 ALTER TABLE `refs` DISABLE KEYS */;
INSERT INTO `refs` VALUES (1,'unisex','unisex','gender','en'),(2,'women','women','gender','en'),(3,'woman','women','gender','en'),(32,'baby','baby','age','en'),(5,'men','men','gender','en'),(6,'man','men','gender','en'),(31,'kid','kids','age','en'),(8,'girls','girls','gender','en'),(9,'girl','girls','gender','en'),(10,'boys','boys','gender','en'),(11,'boy','boys','gender','en'),(12,'tee','t-shirt','apparel','en'),(33,'one-piece','one-piece','apparel','en'),(14,'shirt','shirt','apparel','en'),(15,'blue','blue','color','en'),(16,'lemon','yellow','color','en'),(17,'fitted','fitted','fit','en'),(18,'denim','denim','material','en'),(19,'tote','bag','apparel','en'),(20,'bag','bag','apparel','en'),(21,'white','white','color','en'),(22,'juniors','girls','gender','en'),(27,'ash','grey','color','en'),(24,'juniors','juniors','age','en'),(25,'junior','juniors','age','en'),(30,'kids','kids','age','en'),(28,'asphalt','grey','color','en'),(29,'organic','cotton','material','en'),(34,'light-pink','pink','color','en'),(35,'youth','youth','age','en'),(36,'silver','grey','color','en'),(37,'t-shirt','t-shirt','apparel','en'),(38,'orange','orange','color','en'),(39,'orange','orange','color','fr'),(40,'bikini','swimsuit','apparel','en'),(41,'black','black','color','en'),(42,'en','en','lang','en'),(43,'es','es','lang','es'),(44,'fr','fr','lang','fr'),(45,'de','de','lang','de'),(46,'jp','jp','lang','jp'),(47,'hr','hr','lang','hr'),(48,'cz','cz','lang','cz'),(49,'us','us','country','en'),(50,'au','au','country','en'),(51,'nz','nz','country','en'),(52,'ca','ca','country','en'),(53,'uk','uk','country','en'),(54,'ie','ie','country','en'),(55,'be','be','country','fr');
/*!40000 ALTER TABLE `refs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-14  1:19:56
