SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `favourite` int(11) DEFAULT '0',
  `thumb_url` text NOT NULL,
  `image_link` text NOT NULL,
  `product_url` text,
  `view_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isFrame` tinyint(4) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `flickr_img_id` varchar(80) NOT NULL,
  `flickr_title` text NOT NULL,
  `google_product_category` varchar(300) NOT NULL,
  `link` varchar(300) NOT NULL,
  `additional_image_link` varchar(300) NOT NULL,
  `condition` varchar(300) NOT NULL,
  `regular_price` double NOT NULL,
  `sale_price_effective_date` date NOT NULL,
  `sale_price` double NOT NULL,
  `brand` varchar(300) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `age_group` varchar(100) NOT NULL,
  `color` varchar(200) NOT NULL,
  `size` varchar(200) NOT NULL,
  `item_group_id` int(11) NOT NULL,
  `material` varchar(300) NOT NULL,
  `pattern` varchar(300) NOT NULL,
  `tax` varchar(300) NOT NULL,
  `shipping` varchar(300) NOT NULL,
  `shipping_weight` varchar(300) NOT NULL,
  `custom_label_0` varchar(300) NOT NULL,
  `custom_label_2` varchar(300) NOT NULL,
  `custom_label_3` varchar(300) NOT NULL,
  `custom_label_4` varchar(300) NOT NULL,
  `loyalty_points` varchar(300) NOT NULL,
  `excluded_destinations` varchar(300) NOT NULL,
  `expiration_date` date NOT NULL,
  `adult` varchar(300) NOT NULL,
  `sale_price_effective_day` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=314995 ;

INSERT INTO `images` (`id`, `title`, `cat_id`, `store_id`, `name`, `brand_id`, `favourite`, `thumb_url`, `image_link`, `product_url`, `view_count`, `created_at`, `isFrame`, `description`, `flickr_img_id`, `flickr_title`, `google_product_category`, `link`, `additional_image_link`, `condition`, `regular_price`, `sale_price_effective_date`, `sale_price`, `brand`, `gender`, `age_group`, `color`, `size`, `item_group_id`, `material`, `pattern`, `tax`, `shipping`, `shipping_weight`, `custom_label_0`, `custom_label_2`, `custom_label_3`, `custom_label_4`, `loyalty_points`, `excluded_destinations`, `expiration_date`, `adult`, `sale_price_effective_day`) VALUES
(1066, '', 34, 34, 'mango gem pock', 12, 0, 'http://tasks.brainiacstech.com/background/images/1385463278.jpg', 'http://tasks.brainiacstech.com/background/images/1385463278.jpg', 'http://asos.com/Mango-Gem-Pocket-Sweat-Top/zkfs0/?iid=2879444&amp;cid=11321&amp;Rf-800=-1,42&amp;sh=0&amp;pge=0&amp;pgesize=-1&amp;sort=-1&amp;clr=Navy&amp;mporgp=L01hbmdvL01hbmdvLUdlbS1Qb2NrZXQtU3dlYXQtVG9wL1Byb2Qv', 9, '2013-12-27 08:06:52', 0, 'Mango Gem Pocket Sweat Top at asos.com', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00'),
(1067, '', 34, 0, 'Mango Crew Neck Knit Top ', 12, 15, 'http://tasks.brainiacstech.com/background/images/1385463800.jpg', 'http://tasks.brainiacstech.com/background/images/1385463800.jpg', 'http://asos.com/Mango-Crew-Neck-Knit-Top/113q7l/?iid=3226475&amp;cid=2623&amp;sh=0&amp;pge=0&amp;pgesize=36&amp;sort=-1&amp;clr=Off+white&amp;mporgp=L01hbmdvL01hbmdvLUNyZXctTmVjay1Lbml0LVRvcC9Qcm9kLw..', 9, '2013-11-29 08:06:52', 0, 'Mango Crew Neck Knit Top at asos.com', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00'),
(1068, '', 34, 0, 'Mango Gem Pocket Sweat Top', 12, 7, 'http://tasks.brainiacstech.com/background/images/1385464125.jpg', 'http://tasks.brainiacstech.com/background/images/1385464125.jpg', 'http://asos.com/mt/www.asos.com/Mango/Mango-Gem-Pocket-Sweat-Top/Prod/pgeproduct.aspx?iid=2743013&amp;cid=11321&amp;sh=0&amp;pge=1&amp;pgesize=12&amp;sort=-1&amp;clr=Navy', 8, '2013-11-29 08:06:52', 0, 'Mango Gem Pocket Sweat Top', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00'),
(1069, '', 34, 0, 'Mango Gem Pocket Sweat Top', 12, 0, 'http://tasks.brainiacstech.com/background/images/1385464507.jpg', 'http://tasks.brainiacstech.com/background/images/1385464507.jpg', 'http://asos.com/mt/www.asos.com/Mango/Mango-Gem-Pocket-Sweat-Top/Prod/pgeproduct.aspx?iid=2743013', 1009, '2013-11-29 08:06:52', 0, 'Mango Gem Pocket Sweat Top', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
