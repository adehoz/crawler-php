delimiter //
CREATE PROCEDURE trendbot (IN min_id INT, IN max_id INT)
BEGIN
    DECLARE update_count INT;
    DECLARE generated_image_id DOUBLE;
    DECLARE id_exists INT;
    SET update_count = 60;
    
    WHILE update_count > 0 DO
        SET generated_image_id = 
            ROUND(RAND() * (max_id - min_id) + min_id);
        SELECT id INTO id_exists FROM images WHERE id = generated_image_id;
        IF id_exists IS NOT NULL THEN
            UPDATE images
                SET favourite = favourite + 1 WHERE id = generated_image_id;
            SET update_count = update_count - 1;
        END IF;
    END WHILE;
END//
delimiter ;