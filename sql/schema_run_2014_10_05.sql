alter table domains
    add column image_tag varchar(4000) after price_tag,
    add column image_callback varchar(50) after image_tag;
