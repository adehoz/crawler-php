alter table domains
    add column date_pricecheck datetime after process_id,
    add column control_pricecheck varchar(32) after date_pricecheck,
    add column price_modified int(1) after control_pricecheck;

alter table pages
    add column date_pricecheck datetime after date_classified,
    add column control_pricecheck varchar(32) after control;
    
drop index pages_dp on pages;
create index pages_dpc on pages(date_pricecheck);
create index pages_cpc on pages(control_pricecheck);
create index pages_dps on pages(date_processed, sitemap_id);

create index domains_dpc on domains(date_pricecheck);
create unique index domains_cpc on domains(control_pricecheck);

delimiter //
create procedure pricecheck_select(
    in v_control varchar(32),
    in v_limit   int(6)
)
begin
    declare p_id int;
    declare done int default 0;
    
    declare cur cursor for
        select p.id
        from pages p, sitemaps s, domains d
        where p.sitemap_id <> 0
        and p.sitemap_id = s.id
        and s.domain_id = d.id
        and d.control_pricecheck = v_control
        -- and d.date_pricecheck < subdate(now(), 1)
        order by p.date_pricecheck asc
        limit v_limit;
    declare continue handler for not found set done = 1;

    -- reserve domain (to avoid multi-access issues)
    update domains
    set control_pricecheck = v_control,
    date_pricecheck = now()
    order by date_pricecheck asc
    limit 1;
    
    -- update corresponding pages with the same control information    
    open cur;
    repeat
        fetch cur into p_id;
        update pages set
            control_pricecheck = v_control,
            date_pricecheck = now()
            where id = p_id;
    until done = 1
    end repeat;
    close cur;
    
    -- return product pages for check
    select id, page_url, image_url, price, currency
    from pages
    where control_pricecheck = v_control;
    
end//
delimiter ;