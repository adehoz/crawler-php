create table pages (
    id                  int(11)         not null auto_increment,
    page_url            varchar(2000)   not null,
    page_url_md5        varchar(32)     not null,
    page_title          varchar(512),
    image_url           varchar(2000),
    image_url_md5       varchar(32),
    image_alt           varchar(512),
    image_mode          int(1),                         -- 0: alt text,
                                                        -- 1: image size
    image_r_location    varchar(512),
    image_s_location    varchar(512),
    image_width         int(5),
    image_height        int(5),
    price               decimal(8,2),
    currency            varchar(10),
    sitemap_id          int(11)         not null,
    date_processed      datetime,
    date_classified     datetime,
    date_pricecheck     datetime,
    control             varchar(32),
    control_pricecheck  varchar(32),
    product             int(1),                         -- 0: not product
                                                        -- 1: product
    country             varchar(2),
    lang                varchar(2),
    category            varchar(50),
    store               varchar(100),
    primary key (id)
) engine=MyISAM default charset=utf8;

create unique index pages_m on pages(page_url_md5);
create unique index pages_c on pages(control);
create index pages_cpc on pages(control_pricecheck);
create index pages_dps on pages(date_processed, sitemap_id);
create index pages_dc on pages(date_classified);
create index pages_dpc on pages(date_pricecheck);

create table sitemaps (
    id                  int(11)         not null auto_increment,
    sitemap_url         varchar(255)    not null,
    domain_id           int(11)         not null,
    primary key (id)
) engine=MyISAM default charset=utf8;

create unique index sitemaps_s on sitemaps(sitemap_url);

create table domains (
    id                  int(11)         not null auto_increment,
    domain_url          varchar(255)    not null,
    search_method       int(1),                         -- 0: robots.txt
                                                        -- 1: sitemap.xml
                                                        -- 2: crawl
    status              int(1),                         -- null: undefined 
                                                -- (available for crawling)
                                                        -- 0: started (other 
                                                -- crawlers should skip it)
                                                        -- 1: finished (other 
                                                -- crawlers should skip it)
    process_id          int(11),
    date_pricecheck     datetime,
    control_pricecheck  varchar(32),
    price_modified      int(1),                     -- 0: no change
                                                    -- 1: price change detected
    created             datetime,
    modified            timestamp       default current_timestamp on update 
current_timestamp,
    primary key (id)
) engine=MyISAM default charset=utf8;

create unique index domains_d on domains(domain_url);
create index domains_dpc on domains(date_pricecheck);
create unique index domains_cpc on domains(control_pricecheck);


-- classifier
create table refs (
  id            int(11) NOT NULL auto_increment,
  word          varchar(50) NOT NULL,
  reference     varchar(50),
  category      varchar(50),
  lang          varchar(2),
  primary key (id)
) engine=MyISAM default charset=utf8;

insert into refs values
    (null, 'en', 'en', 'lang', 'en'),
    (null, 'es', 'es', 'lang', 'es'),
    (null, 'fr', 'fr', 'lang', 'fr'),
    (null, 'de', 'de', 'lang', 'de'),
    (null, 'jp', 'jp', 'lang', 'jp'),
    (null, 'hr', 'hr', 'lang', 'hr'),
    (null, 'cz', 'cz', 'lang', 'cz'),
    (null, 'us', 'us', 'country', 'en'),
    (null, 'au', 'au', 'country', 'en'),
    (null, 'nz', 'nz', 'country', 'en'),
    (null, 'ca', 'ca', 'country', 'en'),
    (null, 'uk', 'uk', 'country', 'en'),
    (null, 'ie', 'ie', 'country', 'en'),
    (null, 'be', 'be', 'country', 'fr')
;

create index refs_w on refs(word);

-- statistics
create table stats (
  id                    int(11) NOT NULL auto_increment,
  ts                    timestamp default current_timestamp,
  domain_total          int(11),
  domain_processed      int(11),
  sitemap_total         int(11),
  page_total            int(11),
  page_processed        int(11),
  pages_w_images        int(11),
  pages_w_products      int(11),
  PRIMARY KEY (id)
);

delimiter //
create procedure gatherstat()
begin
    declare p_domain_total      int;
    declare p_domain_processed  int;
    declare p_sitemap_total     int;
    declare p_page_total        int;
    declare p_page_processed    int;
    declare p_pages_w_images    int;
    declare p_pages_w_products  int;
    
    select count(*) from domains into p_domain_total;
    select count(*) from domains where status = 1 into p_domain_processed;
    select count(*) from sitemaps into p_sitemap_total;
    select count(*) from pages into p_page_total;
    select count(*) from pages where date_processed is not null into 
p_page_processed;
    select count(*) from pages where image_url is not null into 
p_pages_w_images;
    select count(*) from pages where product is not null into 
p_pages_w_products;

    insert into stats values (
        null,
        null,
        p_domain_total,
        p_domain_processed,
        p_sitemap_total,
        p_page_total,
        p_page_processed,
        p_pages_w_images,
        p_pages_w_images
    );

end//
delimiter ;


-- !!! superseeded in schema_run_2014_10_26.sql
delimiter //
create procedure pricecheck_select(
    in v_control varchar(32),
    in v_limit   int(6)
)
begin
    declare p_id int;
    declare done int default 0;
    
    declare cur cursor for
        select p.id
        from pages p, sitemaps s, domains d
        where p.sitemap_id <> 0
        and p.sitemap_id = s.id
        and s.domain_id = d.id
        and d.control_pricecheck = v_control
        and d.date_pricecheck < subdate(now(), 1)
        order by p.date_pricecheck asc
        limit v_limit;
    declare continue handler for not found set done = 1;

    -- reserve domain (to avoid multi-access issues)
    update domains
    set control_pricecheck = v_control,
    date_pricecheck = now()
    order by date_pricecheck asc
    limit 1;
    
    -- update corresponding pages with the same control information    
    open cur;
    repeat
        fetch cur into p_id;
        update pages set
            control_pricecheck = v_control,
            date_pricecheck = now()
            where id = p_id;
    until done = 1
    end repeat;
    close cur;
    
    -- return product pages for check
    select id, page_url, image_url, price, currency
    from pages
    where control_pricecheck = v_control;
    
end//
delimiter ;