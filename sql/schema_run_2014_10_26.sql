alter table images
    add column date_pricecheck  datetime,
    add column control_pricecheck varchar(32);

create index images_cpc on images(control_pricecheck);
create index images_dpc on images(date_pricecheck);

delimiter //
create procedure pricecheck_select(
    in v_control varchar(32),
    in v_limit   int(6)
)
begin
    declare p_id int;
    declare done int default 0;
    
    -- retrieve v_limit rows from images that optionally match v_domain.
    -- ignore rows that have date_pricecheck newer than yesterday.
    declare cur cursor for
        select id
        from images
        where date_pricecheck < subdate(now(), 1)
        or date_pricecheck is null
        order by date_pricecheck asc
        limit v_limit;
    declare continue handler for not found set done = 1;
    
    -- update control information
    open cur;
    repeat
        fetch cur into p_id;
        update images set
            control_pricecheck = v_control,
            date_pricecheck = now()
            where id = p_id;
    until done = 1
    end repeat;
    close cur;
    
    -- return product pages for check
    select id, product_url, regular_price, sale_price, currency
    from images
    where control_pricecheck = v_control;
    
end//
delimiter ;