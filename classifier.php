<?php

require_once('config.inc.php');
require_once('common.inc.php');
require_once('dectree.php');

class Classifier
{

    // Properties.
    
    private $_common = null;            // common class instance

    private $_dbh = null;               // database handler
    private $_tokens = array();         // relevant tokens from url and alt
    private $_class = array();          // classification array:
                                        // categories -> references
    private $_attributes = array();     // filled from the DecisionTree class
    private $_languages = array();      // count token languages
    private $_country = '';             // product country
    private $_store = '';               // store name (from site URL)
    private $_category = '';            // apparel category
    
    private $_origin_table = '';        // source table of the product data
    private $_origin_id = '';           // id of the product data in the table


    // Constructor.
    public function __construct() {
        $this->_common = new Common();
    }

    // Set debug level, -1 for PRODUCTION!
    public function debug($level) {
        $this->_common->set_debug($level);
    }
    
    // Sets database parameters.
    public function set_db($dbconn, $dbuser, $dbpass) {
        $this->_dbh = new Database($dbconn, $dbuser, $dbpass);
    }
    
    public function set_attributes($attributes) {
        $this->_attributes = $attributes;
    }
    
    // For a given word returns a reference word and a category (may be
    // multiple rows).
    private function _lookup($word) {
    
        $q = <<<QUERY
SELECT reference, category, lang
FROM refs
WHERE word = :word
QUERY;
        try {
            $sth = $this->_dbh->handler->prepare($q);
            $sth->bindParam(":word", $word);
            $sth->execute();
            $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        
        catch (PDOException $e) {
            fwrite(STDERR, "Error of reference lookup: " . $e->getMessage()
                . " [" . $e->getCode() . "]\n");
            return NULL;
        }
        
        return $rows;
    }
    
    // Create classification hash for a given token.
    private function _classification($ref) {
    
        if (!isset($ref['reference'])) {
            return 0;
        }
        
        printf("%s <--- %s [%s]\n", $ref['reference'], $ref['category'],
            $ref['lang']);
            
        // We are especially interested in category = apparel, as this
        // is the main category for this classifier.
        if ($ref['category'] == 'apparel') {
            $this->_category = $ref['reference'];
        }
        
        return 1;
    }
    
    // Get tokens from the table: product URL, image URL, ALT text.
    public function product_info_tokens() {

        $row = $this->_dbh->get_product_data('pages', 'date_classified');
        if (! isset($row)) {
            $this->_tokens = array();
            return 0;
        }
        
        // save origin information
        $this->_origin_table = 'pages';
        $this->_origin_id = $row['id'];
        
        $this->_common->writeit(1, "PAGE URL:   " . $row['page_url']);
        $this->_common->writeit(1, "PAGE TITLE: " . $row['page_title']);
        $this->_common->writeit(1, "IMAGE URL:  " . $row['image_url']);
        $this->_common->writeit(1, "ALT TEXT:   " . $row['image_alt']);
        
        $page_url_tokens = $this->_common->tokens('url',    $row['page_url']);
        $page_title_tokens = $this->_common->tokens('text', $row['page_title']);
        $image_url_tokens = $this->_common->tokens('url',   $row['image_url']);
        $alt_text_tokens = $this->_common->tokens('text',   $row['image_alt']);
        
        $tokens = array_merge(
            $page_url_tokens,
            $page_title_tokens,
            $image_url_tokens,
            $alt_text_tokens
        );
        $tokens_p = join(' ', $tokens);
        $this->_common->writeit(2, "Tokens: [" . $tokens_p . "]");
        
        // Get store.
        $url_arr = parse_url($row['page_url']);
        $domain_arr = explode('.', $url_arr['host']);
        $this->_store = $domain_arr[count($domain_arr) - 1 - 1];
        
        // Delete empty value if exists. Might occur on getting tokens from url
        if (($key = array_search('', $tokens)) !== FALSE) {
            unset($tokens[$key]);
        }
        
        $this->_tokens = array_map('strtolower', array_unique($tokens));
        return 1;
    }
    
    // Looks up classification for each word.
    public function classify() {
        foreach ($this->_tokens as $token) {
            $refs = $this->_lookup($token);
            foreach ($refs as $ref) {
                if (!$this->_classification($ref)) {
                    $this->_common->writeit
                        (2, "No classification information for $token");
                    continue;
                }
                
                if (! isset($this->_class[$ref['category']])) {
                    $this->_class[$ref['category']] = array($ref['reference']);
                }
                elseif (! in_array($ref['reference'], 
                    $this->_class[$ref['category']])) {
                    $this->_class[$ref['category']][] = $ref['reference'];
                }
                
                // Add reference language to languages array to keep track
                // of the most common one.
                if (isset($ref['lang'])) {
                
                    $weight = 1;
                    if (isset($ref['category'])) {
                        if ($ref['category'] == 'lang'
                        || $ref['category'] == 'country') {
                            $weight = 3;
                        }
                    }
                
                    if (isset($this->_languages[$ref['lang']])) {
                        $this->_languages[$ref['lang']]++;
                    }
                    else {
                        $this->_languages[$ref['lang']] = 1;
                    }
                }
                
                // Get country info.
                if ($ref['category'] == 'country') {
                    $this->_country = $ref['reference'];
                }
            }
        }
        
        return $this->_class;
    }
    
    public function testcases() {

        $testcase = array();
        
        foreach ($this->_attributes as $attribute) {
        
            if (isset($this->_class[$attribute][0])) {
                $testcase[$attribute] = $this->_class[$attribute][0];
            }
            
            else {
                $testcase[$attribute] = "_not_set";
            }
        }
        
        return $testcase;
    }
    
    public function update_product_status($outcome) {
    
        if (is_null($outcome)) {
            $outcome = 'undefined';
        }
        
        // Mapping.
        $OUTCOME = array(
            'n'         => 0,
            'p'         => 1,
            'undefined' => -1
        );
        
        // Get language (key of a language array with maximum number
        // of appearances).
        $language = $this->_determine_language();
        
        // Get country, either determined from tokens or country is the same
        // as the language.
        $country = $this->_country != '' ? $this->_country : $language;
        
        // debug info.
        $this->_common->writeit(2, "country: " . $country . ", language: "
            . $language . ", store: " . $this->_store . ", category: "
            . $this->_category);
        
        $table = $this->_origin_table;
        $id = $this->_origin_id;
        
        $q = <<<QUERY
UPDATE $table
SET
    product = :outcome,
    country = :country,
    lang = :language,
    store = :store,
    category = :category
WHERE id = :id;
QUERY;
        
        $sth = $this->_dbh->handler->prepare($q);
        $sth->bindParam(":outcome", $OUTCOME[$outcome]);
        $sth->bindParam(":country", $country);
        $sth->bindParam(":language", $language);
        $sth->bindParam(":store", $this->_store);
        $sth->bindParam(":category", $this->_category);
        $sth->bindParam(":id", $id);
        $rv = $sth->execute();
        if ($rv === FALSE) {
            fwrite("ERROR: Failed to update $table ($q): "
                . $sth->getMessage() . "\n");
        }
    }
    
    private function _determine_language() {
    
        // Language is determined as the key in the array with the highest
        // value count. Return null if array is empty.
        
        if (empty($this->_languages)) {
            return null;
        }

        return array_search(max($this->_languages), $this->_languages);    
    }

}

$opts = getopt("1");

// For the classifier to work, first we need to train the classification
// using decision tree learning.
$dt = new DecisionTree();
$set = $dt->load_training_set();
if (empty($set)) {
    fwrite(STDERR, "Training set empty.\n");
    exit(1);
}
$attributes = $dt->get_attributes();
$dt->root_node_entropy();
$tree = $dt->learn($set, $attributes);

// Now we iterate through products, trying to get identification tokens
// from page url, image url and image alt text. Using these tokens,
// run a decision tree choice. Save the result for each link.

while (1) {
    $classifier = new Classifier();
    $classifier->debug(DEBUG_LEVEL);
    $classifier->set_db(DBCONN, DBUSER, DBPASS);

    if (! $classifier->product_info_tokens()) {
        break;
    }
    $classifier->classify();
    $classifier->set_attributes($attributes);
    $tc = $classifier->testcases();
    print_r($tc);
    $outcome = $dt->test($tree, $tc);
    if (is_null($outcome)) {
        print "Not classified.\n";
    }
    print "Outcome: $outcome\n";
    $classifier->update_product_status($outcome);
    
    if (isset($opts['1'])) {
        break;
    }
}

?>