<?php

require_once('config.inc.php');
require_once('common.inc.php');

$c = new Common();
$c->set_debug(DEBUG_LEVEL);

function get_image_id_range($dbh, $c) {

    $q = "SELECT MIN(id) as min_id, MAX(id) AS max_id FROM images";
    $sth = $dbh->handler->prepare($q);
    $sth->execute();
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function run_trendbot($dbh, $id_range, $c) {

    $c->writeit(2, "RUNNING TRENDBOT.");
    $q = "CALL trendbot(" . $id_range['min_id'] 
        . "," . $id_range['max_id'] . ")";
    $sth = $dbh->handler->prepare($q);
    $rv = $sth->execute();
    
    if (FALSE === $rv) {
        $c->writeit(1, "FAILED TO EXECUTE TRENDBOT.");
        $c->writeit(1, "Error code: " . $sth->errorCode());
        $dbh = null;
        exit(1);
    }
}

$dbh = new Database(DBCONN, DBUSER, DBPASS);
$id_range = get_image_id_range($dbh, $c);
$c->writeit(1, "MIN ID: " . $id_range['min_id'] 
    . ", MAX ID: " . $id_range['max_id']);
while (1) {
    run_trendbot($dbh, $id_range, $c);
    sleep(60);
}

/*
delimiter //
CREATE PROCEDURE trendbot (IN min_id INT, IN max_id INT)
BEGIN
    DECLARE update_count INT;
    DECLARE generated_image_id DOUBLE;
    DECLARE id_exists INT;
    SET update_count = 60;
    
    WHILE update_count > 0 DO
        SET generated_image_id = 
            ROUND(RAND() * (max_id - min_id) + min_id);
        SELECT id INTO id_exists FROM images WHERE id = generated_image_id;
        IF id_exists IS NOT NULL THEN
            UPDATE images
                SET favourite = favourite + 1 WHERE id = generated_image_id;
            SET update_count = update_count - 1;
        END IF;
    END WHILE;
END//
delimiter ;

*/

?>

