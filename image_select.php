<?php

/******************************************************************************
 * Program: Image Select
 * Author:  Domagoj Marsic <domagoj.marsic@gmail.com>
 * Version: 1.1
 * Created: Jan 28 2014
 * Updated: Feb 15 2014
 *
 * Searches a table with product links for the unprocessed URLs. For such URL
 * it finds the best image (by comparing URL tokens to IMG ALT content) and
 * updates image data in the database.
 *
 * Works in two "modes", based on the mode the URL was obtained: sitemap or
 * crawler. The difference only deals with different database tables.
 *
 * Recommended usage, in a loop:
 *   $product = new product({'crawler'|'sitemap'});
 *   $product->set_db($dbconn, $dbuser, $dbpass);
 *   $product->find_next_url('file', $URLSOURCE); // or find_next_url_phpcrawl
 *   $product->set_url_tokens();
 *   $product->get_product_image
 *   $product->store_image_data
*****************************************************************************/

require_once('config.inc.php');
require_once('common.inc.php');

class Product
{
    // Properties.
    private $_domain_filter = '';       // process matched domain(s)
    private $_url = '';                 // product url
    private $_image_url = '';           // product image url
    private $_currency = '';            // product price currency
    private $_price = null;             // product price
    private $_content = '';             // product page url
    private $_page_title = '';          // product page title
    private $_url_tokens = array();     // image url tokens
    private $_alt_tokens = array();     // image alt tokens
    private $_img_alt_text = '';        // image alt text (to be saved)
    private $_img_mode = '';            // best image determined by (alt|size)

    private $_common = null;            // common class instance
    
    private $_dbh = null;               // database handler
    private $_dbdata = array();         // product data from db
    
    private $_thumb_max_height = 150;   // maximum thumbnail height (in px)
    private $_thumb_max_width = 150;    // maximum thumbnail width (in px)
    private $_thumb_quality = 75;       // thumbnail JPEG quality (0 - 100)
    private $_img_quality = 80;         // image JPEG quality (0 - 100)
    private $_thumb_path = '';          // path on the local system for thumb
    private $_img_path = '';            // path on the local system for image
    private $_img_base = '';            // base directory for images.
    
    private $_currencies = array(
        '\$', 'USD', '€', 'EUR'
    );
    
    private $_img_decision = array(
        'ALT'   => 0,
        'SIZE'  => 1
    );

    // Constructor.
    public function __construct() {
        $this->_common = new Common();
    }
    
    // Returns a list of tokens from a given string.
    // Leave out hostname.
    private function _get_tokens($string) {
        $tokens = preg_split("/[\W\-]+/", $string);
        return $tokens;
    }
    
    // Matches two arrays (tokens from url and tokens from image alt attr).
    // Returns number in first, second array and a number of matches.
    private function _match_tokens() {
        
        // delete empty strings.
        $empty = "";
        if (($key = array_search($empty, $this->_url_tokens)) !== false) {
            unset($this->_url_tokens[$key]);
        }
        if (($key = array_search($empty, $this->_alt_tokens)) !== false) {
            unset($this->_alt_tokens[$key]);
        }
    
        // get match of two arrays.
        $match_arr = array_intersect($this->_url_tokens, $this->_alt_tokens);
        return array(
            'match' => count($match_arr),
            'url_tokens' => count($this->_url_tokens),
            'alt_tokens' => count($this->_alt_tokens)
        );
    }
    
    // Fixes image link to a get full URL.
    private function _correct_image_link($image_link) {
        $page_url_arr = parse_url($this->_url);
        $image_url_arr = parse_url($image_link);
        $corrected_image_url = $image_link;
        
        // Full url is built from the page hostname.
        if (! isset($image_url_arr['host'])) {
        
            // Absolute path for image.
            if (substr($image_url_arr['path'], 0, 1) == '/') {
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $image_url_arr['path'];
            }
            
            // Relative path for image.
            else {
                // Remove leading dot if exists.
                if (substr($image_url_arr['path'], 0, 1) == '.') {
                    $image_url_arr['path'] = substr($image_url_arr['path'], 1);
                }
                
                $dir = implode('/', explode('/', $page_url_arr['path'], -1));
                if ($dir == '') {
                    $dir = '/';
                }
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $dir . $image_url_arr['path'];
            }        
        }
        
        // Add http:// if scheme not set
        $image_url_arr = parse_url($corrected_image_url);
        if (! isset($image_url_arr['scheme'])) {
        
            $corrected_image_url = $image_link;
            
            // Add http:// if scheme not set.
            $corr_url_arr = parse_url($corrected_image_url);
            if (! isset($corr_url_arr['scheme'])) {
                $corrected_image_url = trim($corrected_image_url, "/");
                $corrected_image_url = "http://" . $corrected_image_url;
            }
        }
        
        return $corrected_image_url;
    }

    // Set debug level, -1 for PRODUCTION!
    public function debug($level) {
        $this->_common->set_debug($level);
    }
    
    // Searches for a random URL from the input file.
    public function find_next_url($products_file) {
    
        $urls = file(
            $products_file, FILE_SKIP_EMPTY_LINES);
        if ($urls === FALSE) {
            return 1;
        }
        
        $line_count = count($urls);
        if ($line_count == 0) {
            return 0;
        }
        $url_idx = rand(0, $line_count - 1);
        $random_url = rtrim($urls[$url_idx]);
        $this->_dbdata['page_url'] = $random_url;
        
        $rewritten = array_merge(
            array_slice($urls, 0, $url_idx),
            array_slice($urls, $url_idx + 1)
        );
        
        if ($line_count == 1) {
            $rewritten = $urls;
        }
        
        file_put_contents($products_file, $rewritten);
        return 1;
    }
   
    // Product URL is tokenized as this is the primary way to get the
    // corresponding product image.
    public function set_url_tokens() {
        if (! isset($this->_dbdata['page_url'])) {
            return 0;
        }
        
        $url = $this->_dbdata['page_url'];

        // Tokenize URL. Exit if 'path' component not set (domain url)
        $tokens = parse_url($url);
        if (! isset($tokens['path'])) {
            return 0;
        }
        
        $this->_url = urldecode($url);
        $this->_url_tokens = $this->_get_tokens($tokens['path']);
        $this->_url_tokens = array_map('strtolower', $this->_url_tokens);
        
        $this->_common->writeit(0, "PRODUCT URL: " . $url);
        printf("%-30s%s\n", "PRODUCT URL:", $url);
        printf("%-30s[%s]\n\n", "URL TOKENS:", join(", ", $this->_url_tokens));
        return 1;
    }
    
    // Read the URI content into a string.
    public function get_content() {
        $this->_common->writeit(1, "Accessing page " . $this->_url);
        $this->_content = file_get_contents($this->_url);
        if ($this->_content === false) {
            printf(STDERR, "Could not get " . $this->_url . "\n");
            return 0;
        }
        return 1;
    }
    
    // Get page title. Will be saved with the image info.
    public function get_page_title() {
        $title = '';
        $content_clean = preg_replace('/[\n]+/', '', $this->_content);
        preg_match('/<\s*title\s*>\s*(.*?)\s*<\s*\/title\s*>/i', 
            $content_clean, $matches);  // a bit paranoid on whitespaces here.
        if (isset($matches[1])) {
            $title = $matches[1];
        }
        $this->_page_title = $title;
        $this->_common->writeit(1, "Page title: " . $title);
        printf("%-30s%s\n", "PAGE TITLE:", $this->_page_title);
        print("Page title tokens are not considered while matching image.\n\n");
    }
   

    // Find all images in the URL content, then find the correct one for
    // the product based on the criteria.
    // Criteria:
    // 1. URL token / alt token match
    // 2. image size
    public function get_product_image_and_price() {
        
        // Use DOM to parse and search for images.
        // libxml_use_internal_errors() used to disable passing of errors
        // and warnings from libxml to PHP.
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($this->_content);
        libxml_clear_errors();
        
        $img_tags = $dom->getElementsByTagName('img');
        $best_token_match = array(
            'match' => 0,
            'url_tokens' => 0,
            'alt_tokens' => 0
        );
        $largest_image_size = 0;
        
        // 0. RULE: Ignore images that are not on the same server.
        // Implemented in rules below.
        // Exceptions exist! Check _correct_image_link()
        
        // 1. Find image based on URL token / alt token match
        $image_alt = '';
        $image_tag = null;
        $count = 0;
        foreach ($img_tags as $tag) {
            $image_link = $tag->getAttribute('src');
            printf("%4d|%-25s%s\n", $count, "IMAGE URL (by token):",
                $image_link);
            
            $corr_image_link = $this->_correct_image_link($image_link);
            
            if (! isset($corr_image_link)) {
                $this->_common->writeit(2, "Failed on rule 0 for "
                    . $image_link);
                    
                print "Image URL is not on the same site as the product.\n";
                continue;
            }
            
            printf("%4d|%-25s%s\n", $count, "CORRECTED IMAGE URL:",
                $corr_image_link);
            
            $image_alt = $tag->getAttribute('alt');
            $this->_alt_tokens = $this->_get_tokens($image_alt);
            $this->_alt_tokens = array_map('strtolower', $this->_alt_tokens);
            
            printf("%4d|%-25s%s\n", $count, "IMAGE ALT:", $image_alt);
            printf("%4d|%-25s[%s]\n", $count, "IMAGE ALT TOKENS:",
                join(", ", $this->_alt_tokens));

            $token_match = $this->_match_tokens();
            printf("%4d|[matched tokens : page tokens : image alt tokens]"
                . " = [%d : %d : %d]\n", $count, $token_match['match'],
                $token_match['url_tokens'], $token_match['alt_tokens']);
            if ($token_match['match'] > $best_token_match['match']) {
                $best_token_match['match'] = $token_match['match'];
                $best_token_match['url_tokens'] = $token_match['url_tokens'];
                $best_token_match['alt_tokens'] = $token_match['alt_tokens'];
                $this->_image_url = $corr_image_link;
                $this->_img_alt_text = $image_alt;
                $this->_img_mode = $this->_img_decision['ALT'];
                $image_tag = $tag;
                printf("%4d|This is currently the best match.\n", $count);
            }
            
            print "\n";
            $count++;
        }
        
        $match_pct = $best_token_match['url_tokens'] > 0
            ? $best_token_match['match'] * 100 / $best_token_match['url_tokens']
            : 0;
        $threshold_pct = 50;
        $threshold_tokens = 2;
        if ($match_pct >= $threshold_pct) {
            print "\tBest match (matching tokens): ";
            print $best_token_match['url_tokens'] . ', '
                . $best_token_match['alt_tokens'] . ', '
                . $best_token_match['match'] . "\n";
        }
        
        if (! ($match_pct >= $threshold_pct
        && $best_token_match >= $threshold_tokens)) {
            print "\tThreshold values not passed.\n";
            $this->_image_url = '';
        }

        // 2. Find image based on largest size (excludes display: none)
        // Activated if percent of matches from the first method is less
        // than $threshold_pct percent.

        /*
        $count = 0;
        if ($match_pct < $threshold_pct) {
                
            foreach ($img_tags as $tag) {
                $image_style = $tag->getAttribute('style');
                if (preg_match("/display\s*:\s*none/i", $image_style)) {
                    continue;
                }
                $image_link = $tag->getAttribute('src');
                printf("%4d|%-25s%s\n", $count, "IMAGE URL (by token):",
                    $image_link);

                $corr_image_link = $this->_correct_image_link($image_link);
                $this->_common->writeit(2, "Image link: " . $image_link
                    . ", Corrected: " . $corr_image_link);
                if (! isset($corr_image_link)) {
                    print "Image URL is not on the same site as the product.\n";
                    continue;
                }
                printf("%4d|%-25s%s\n", $count, "CORRECTED IMAGE URL:",
                    $corr_image_link);
            
                $image_size = @getimagesize($corr_image_link);
                $size_b = $image_size[0] * $image_size[1];
                printf("%4d|%-25s%d B\n", $count, "SIZE:", $size_b);
                
                if ($size_b > $largest_image_size) {
                    $largest_image_size = $size_b;
                    $this->_image_url = $corr_image_link;
                    printf("%4d|This is currently the best match.\n", $count);
                }
                
                $image_tag = $tag;
            }

            print "\tBest match (by size):   ";
            print $largest_image_size . " bytes\n";
            
            $this->_img_mode = $this->_img_decision['SIZE'];
            $count++;
        }*/
        
        print "\tProduct URL:   " . $this->_url . "\n";
        print "\tImage URL:     " . $this->_image_url . "\n";
        

        // PRICE
        $node = $image_tag;
        $price = null;
        $depth = 0;
        while (isset($node->parentNode)) {
            $node = $node->parentNode;
            $price_arr = $this->_search_price($node);
            if (isset($price_arr)) {
                $this->_currency = $price_arr[0];
                $this->_price = $price_arr[1];
                print "\tProduct price (depth $depth): " . $this->_currency
                    . $this->_price . "\n";
                break;
            }
           
            $depth--;
        }
    }
    
    
    // Recursive helper function that searches for price match in
    // current DOM node and its children nodes.
    private function _search_price($node) {
    
        foreach ($node->childNodes as $child) {
            if (isset($child->textContent)) {
                $text = $child->textContent;

                foreach ($this->_currencies as $curr) {
                    if (preg_match("/($curr)\s*(\d+[\d\.,]+)/", 
                    $text, $matches)) {
                        return array($matches[1], $matches[2]);
                    }
                }
            }
            
            if (isset($child->childNodes)) {
                $price_arr = $this->_search_price($child);
                if (isset($price_arr)) {
                    return $price_arr;
                }
            }
        }
        
        return null;
    }
}

$opts = getopt("1");

$products_file = "images-classifier/products.txt";
$output_file = "images-classifier/images.txt";

// Process products in an infinite loop. Ends when there are no more
// URLs that don't have 'date_processed' set.
while (1) {

    $product = new Product();
    $product->debug(-1);
    
    // Find next available product (from database)
    $rv = $product->find_next_url($products_file);
    if (! $rv) {
        print "No more products available.\n";
        break;
    }
    
    // URL tokens are used as a means of checking the correct product image.
    if (! $product->set_url_tokens()) {
        print "URL not set.\n";
        continue;
    }

    // From the page, get image and save image data to the database.
    if (! $product->get_content()) {
        print "Failed to get URL.\n";
        if (isset($opts['1'])) {
            break;
        }
        continue;
    }
    
    $product->get_page_title();
    
    $product->get_product_image_and_price();
    
    // Exit if "-1" command-line argument was passed. This is to ensure
    // we can run tests with only one page.
    if (isset($opts['1'])) {
        print "Exiting by request.\n";
        break;
    }
}

?>