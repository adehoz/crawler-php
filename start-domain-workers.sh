#!/bin/bash
target_workers=30
existing_workers=`ps -fu $USER | grep "php domain.php" | grep -v grep | wc -l`
let workers=target_workers-existing_workers
while [ $workers -gt 0 ]; do
    php domain.php -m database -c --instance=$workers 1>logs/domain_${workers}_$$.txt 2>&1 &
    #php domain.php --instance=$workers 1>logs/domain_${workers}_$$.txt 2>&1 &
    let workers=workers-1
    sleep 5
done
