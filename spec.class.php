<?php

class MatchImage
{

    // Properties.
    private $_page_url = null;                  // originating page url
    private $_image_tags = null;                // array of image tags
    
    private $_max_image_url = null;             // URL of the largest image
    private $_max_image_size = 0;               // max size in bytes
    private $_max_image_size_count = 0;         // same-sized images counter
    
    
    // Constructor.
    public function __construct($page_url, $content) {

        // Create DOM representation from the page content. We don't need to
        // keep content, only DOM.
        
        $this->_page_url = $page_url;
    
        $content = $content;
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_clear_errors();
        
        $this->_image_tags = $dom->getElementsByTagName('img');
    }
    
    public function image_tags() {
    
        // Return array of image tags. Each tag is an array from DOM.
        return $this->_image_tags;
    }
    
    public function valid_url($tag) {
    
        // Return valid URL from the img tag of the DOM.
    
        $src = $tag->getAttribute('src');
        $valid_image_url = $src;
        $page_url_a = parse_url($this->_page_url);
        
        // Add host
        $image_url_a = parse_url($src);
        if (! isset($image_url_a['host'])) {
            $valid_image_url = $this->_build_path($page_url_a, $image_url_a);
        }
        
        // Add scheme
        $image_url_a = parse_url($valid_image_url);
        if (! isset($image_url_a['scheme'])) {
            $valid_image_url = $this->_add_http_scheme($valid_image_url);
        }

        return $valid_image_url;
    }
    
    public function calc_image_size($url) {
    
        // Calculate image size and compare with the current largest image.
        // Keep track of the number of largest images.
    
        $image_size = @getimagesize($url);
        $size_b = $image_size[0] * $image_size[1];
        
        // Found new largest image.
        if ($size_b > $this->_max_image_size) {
            $this->_max_image_url = $url;
            $this->_max_image_size = $size_b;
            $this->_max_image_size_count = 1;
        }

        elseif ($size_b == $this->_max_image_size) {
            $this->_max_image_size_count++;
        }
    }
    
    public function match_tokens($tag) {
    
        $image_alt = $tag->getAttribute('alt');
        $alt_tokens = $this->_get_tokens($image_alt);
        $alt_tokens = array_map('strtolower', $alt_tokens);
        
        $token_match = $this->_match_tokens($this->_url_tokens, $alt_tokens);
        
    }
    
    // --------------------------------------------------------------------- //
    
    private function _build_path($page_url_a, $image_url_a) {
    
        // Create absolute path for image if url does not contain host.
        
        if (substr($image_url_a['path'], 0, 1) == '/') {
            return (
                $page_url_a['scheme'] . '://' 
                . $page_url_a['host'] . $image_url_a['path']
            );
        }
        
        // Build relative path.

        else {
        
            // Remove leading dot if exists.
            if (substr($image_url_a['path'], 0, 1) == '.') {
                $image_url_a['path'] = substr($image_url_a['path'], 1);
            }
            
            $dir = implode('/', explode('/', $page_url_a['path'], -1));
            if ($dir == '') {
                $dir = '/';
            }
            
            return (
                $page_url_a['scheme'] . '://'
                . $page_url_a['host'] . $dir . $image_url_a['path']
            );
        }
    }
    
    private function _add_http_scheme($url) {
    
        // Adds 'http://' in front of the url.
        return ('http://' . trim($url, "/"));
    }
}


/* Class Spec contains product specification properties and methods.
 * It is derived from the class Product and called from there as well.
 * The main purpose is to determine the product image and price.
 */

class Spec
{

    // Properties.
    private $_url        = '';                         // page url
    private $_content    = '';                         // input content
    private $_price_tag  = '';                         // input price tag
    private $_price      = null;                       // output price
    private $_image      = null;                       // output image
    
    
    
    // Constructor.
    public function __construct($url, $content, $price_tag = '', $mode = 0) {

        // Mode 1: price_tag is set: match price in price tag, search for image.
        // Mode 2: price_tag not set: search for image, search for price in
        //         proximity.
        
        $this->_url = $url;
        $this->_content = $content;
        $this->_price_tag = $price_tag;
        $this->_common = new Common();
        $this->_common->set_debug($mode);
        $this->_common->writeit
            (1, "Content legth: " . strlen($this->_content));
        
        $this->_currencies = $this->_load_currencies();
        
        if (isset($this->_price_tag) && $this->_price_tag != '') {
            $this->_common->writeit(1, "Price tag set");
            $this->_price = $this->_match_price();
            
            if (! is_null($this->_price)) {
                $this->_image = $this->_search_image();
            }
        }
        
        else {
            $node = $this->_search_image();
            $this->_search_price($node);
        }
    }
    
    public function get_price() {
        
        // Return array(value, currency) for price.
        return $this->_price;
    }
    
    public function get_image() {

        // Return array(...) for image.
        return $this->_image;
    }
    
    // --------------------------------------------------------------------- //
    
    private function _load_currencies() {

        // Loads currencies from the file. If file not found, use default list.
        
        $currencies = file_get_contents(CURRENCIES_FILE);
        if (FALSE === $currencies) {
            return;
        }
        $curr_arr = preg_split("/[\s,]+/", trim($currencies));
        return $curr_arr;
    }

    private function _search_image() {
    
        $m = new MatchImage($this->_url, $this->_content);
        
        foreach ($m->image_tags() as $tag) {
            
            $url = $m->valid_url($tag);
            if (is_null($url)) continue;
            
            $m->calc_image_size($url);
            $m->match_tokens($tag);
        }
    
    }
    
    private function _search_price () {}
    
    private function _match_price() {
    
        // Based on the page content, try to match price tag (read from the
        // domains table) to extract the price and the currency.
        // Return array(price, currency) if matched.
        // Return NULL if not matched.
        
        // Declare subject and pattern.
        $subject = $this->_remove_ctrl($this->_content);
        $pattern = $this->_remove_ctrl($this->_price_tag);
    
        // Check pattern.
        $f_c_value = $this->_var_isset($pattern, '_C_VALUE');
        $f_c_curr  = $this->_var_isset($pattern, '_C_CURRENCY');
    
        // Prepare pattern.
        $pattern = $this->_escape_special($pattern);
        $pattern = $this->_replace_custom_vars($pattern);
        
        // Add slashes around the pattern.
        $pattern = '/' . $pattern . '/';
        $this->_common->writeit(2, "PATTERN: " . $pattern);
        
        // Match value and currency
        $price = $this->_match_val_curr
            ($pattern, $subject, $f_c_value, $f_c_curr);

        // Validation.
        if (! isset($price['value'])) {
            $this->_common->writeit(1, "Price tag --> no match (value)");
            return NULL;
        }
        if (! isset($price['currency'])) {
            $this->_common->writeit(1, "Price tag --> no match (currency)");
            return NULL;
        }
        
        $this->_common->writeit(0, "Price tag matched: " .
            $price['value'] . " " . $price['currency']);
        
        return $price;
    }
    
    // --------------------------------------------------------------------- //
    // Helper functions and one-time functions.
    
    // Remove all non-printable characters.
    private function _remove_ctrl($content) {
        return preg_replace('/[\x00-\x1F\x7F]/m', '', $content);
    }
    
    // Escape special characters.
    private function _escape_special($content) {
        return preg_quote($content, '/');
    }
    
    // Replace custom strings in the pattern with the named capture groups.
    private function _replace_custom_vars($pattern) {

        if (FALSE !== strpos($pattern, '_C_CURRENCY')) {
            $pattern = substr_replace($pattern, '(?P<currency>.*?)',
                strpos($pattern, '_C_CURRENCY'), strlen('_C_CURRENCY'));
        }
    
        if (FALSE !== strpos($pattern, '_C_VALUE')) {
            $pattern = substr_replace($pattern, '(?P<value>\d.*?)',
                strpos($pattern, '_C_VALUE'), strlen('_C_VALUE'));
        }
        
        $pattern = str_replace('_C_ANY', '.*?', $pattern);

        return $pattern;
    }
    
    // Determine whether _C_VALUE has been set.
    private function _var_isset($pattern, $string) {
        if (FALSE !== strpos($pattern, $string)) {
            return 1;
        }
        return 0;
    }
    
    // Match value and currency in the content based on the pattern.
    private function _match_val_curr
    ($pattern, $subject, $f_c_value, $f_c_curr) {
    
        $value = null;
        $currency = null;
        
        preg_match($pattern, $subject, $matches);

        // If _C_VALUE has been defined in price_tag
        if ($f_c_value == 1) {
            $value = isset($matches['value']) 
                ? $this->_price_to_number($matches['value']) : null;
        }
        
        // If _C_VALUE has not been defined in price_tag
        else {
            if (isset($matches[0])) {
                $value = $this->_match_val_numeric($matches[0]);
            }
        }
        
        // If _C_CURRENCY has been defined in price_tag
        if ($f_c_curr == 1) {
            $currency = isset($matches['currency']) 
                ? $matches['currency'] : null;
        }
        
        // If _C_CURRENCY has not been defined in price_tag
        else {
            if (isset($matches[0])) {
                $currency = $this->_match_curr($matches[0]);
            }
        }
    
        return array(
            'value' => $value, 
            'currency' => $currency
        );
    }
    
    // (2nd level) match numeric value that might be price from
    // already matched piece of text from the content. This might be
    // extremely unreliable if the price tag is not carefully set because
    // it will match the first numeric from the extract, whether that was
    // the intention or not.
    private function _match_val_numeric($extract) {

        preg_match("/([\d,]+)(\.\d{2})/", $extract, $matches);
        
        if (isset($matches[1])) {
            $value = $this->_price_to_number($matches[1]);
        }
        
        if (isset($matches[2])) {
            $value += $matches[2];
        }
        
        return $value;
    }
    
    
    // Transform price string to a numeric value. \d*\.\d{2}
    private function _price_to_number($price_str) {
        $price_str = str_replace(',', '', $price_str);
        return (floatval($price_str) + 0.00);
    }
    
    // (2nd level) match currency from the defined list of currencies
    // with the extract matched by the price_tag. First currency matched
    // in the given list is returned. As with _match_val_numeric, this
    // is an unreliable method and it's always recommended to use
    // _C_VALUE and _C_CURRENCY variables.
    private function _match_curr($extract) {
    
        foreach ($this->_currencies as $currency) {
            if (FALSE !== strstr($extract, $currency)) {
                return $currency;
            }
        }
        
        return null;
    }

}
?>