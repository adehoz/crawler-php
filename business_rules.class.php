<?php

/*
 * Class BusinessRules is a collection of procedures for validating
 * business rules. Organizing business requirements in this way
 * enables easier manipulation and distinction of technical and
 * business design reasons.
 *
 * These rules are mostly independent, for that reason I treat them
 * as self-contained units and keep all their constants and variables
 * within methods.
 *
 * Created on Nov 14 2014
 */
 
class BusinessRules {


    // No constructor needed at this time.
    public function __construct() {}

    // Sale price should not be less than 70% of the price (Nov 10 2014)
    // Returns True if sales price in given range (ok)
    // Returns False otherwise (not ok - we don't take it as a sale price)
    public function is_sale_price($regular_price, $sale_price) {
        $SALE_PRICE_MIN_PCT = 0.3;
        $SALE_PRICE_MAX_PCT = 1.0;
        if ($sale_price > $SALE_PRICE_MIN_PCT * $regular_price &&
                $sale_price < $SALE_PRICE_MAX_PCT * $regular_price) {
            return TRUE;
        }
        return FALSE;
    }
    
    // Search tokens that describe sale prices in the part of the HTML
    // code between what we consider price and the next numerical value.
    // If this token is matched, the next value is most probably a sale
    // value.
    public function search_price_token($subject) {
        $TOKENS = array('sale', 'discount', 'reduced');
        $subject = strtolower($subject);
        foreach ($TOKENS as $token) {
            if (FALSE !== strstr($subject, $token)) {
                return TRUE;
            }
        }
        return FALSE;
    }
    
}


?>