#!/bin/bash
domain=
if [ -n "$1" ]; then
	domain=`echo " -d " $1 " "`
	echo "Domain set to:" $domain
fi
target_workers=20
existing_workers=`ps -fu $USER | grep "php crawler.php" | grep -v grep | wc -l`
let workers=target_workers-existing_workers
while [ $workers -gt 0 ]; do
    php crawler.php $domain --instance=$workers 1>logs/crawler_s_$workers.txt 2>&1 &
    let workers=workers-1
    sleep 5
done
