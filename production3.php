<?php

require_once('config.inc.php');
require_once('common.inc.php');

define("DEST_DBCONN", DBCONN);
define("DEST_DBUSER", DBUSER);
define("DEST_DBPASS", DBPASS);

define("SRC_TABLE",  "pages");
define("DEST_TABLE", "images");

define("LAST_ID_FILE", ".production_last_id");

#-----------------------------------------------------------------------------#

function get_row($dbh, $last_id) {

    $src_table = SRC_TABLE;

    $q = <<<QUERY
SELECT
    id,
    page_url,
    image_alt,
    image_r_location,
    image_s_location, 
    date_processed,
    currency,
    price
FROM $src_table
WHERE id > $last_id
LIMIT 1;
QUERY;

    $sth = $dbh->handler->prepare($q);
    $sth->execute();
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function put_row($dbh, $row) {

    $dest_table = DEST_TABLE;
    $row['currency'] = translate_currency($row['currency']);
    $row['price'] = is_null($row['price']) ? 0.0 : $row['price'];
    
    $ins = <<<QUERY
INSERT INTO $dest_table
(id, title, cat_id, store_id, thumb_url, image_link, product_url,
view_count, created_at, description, flickr_img_id, flickr_title,
google_product_category, link, additional_image_link, `condition`,
regular_price, sale_price_effective_date, sale_price, brand, gender,
age_group, color, size, item_group_id, material, pattern, tax,
shipping, shipping_weight, custom_label_0, custom_label_2,
custom_label_3, custom_label_4, loyalty_points, excluded_destinations,
expiration_date, adult, sale_price_effective_day, currency)
VALUES
(NULL, '', 0, 0, :thumb_url, :image_link, :product_url,
0, :created_at, :description, '', '',
'', '', '', '',
:price, '1970-01-01', 0.0, '', '',
'', '', '', 0, '', '', '',
'', '', '', '',
'', '', '', '',
'1970-01-01', '', '1970-01-01', :currency)
QUERY;
    $sth = $dbh->handler->prepare($ins);
    $sth->bindParam(":thumb_url",   $row['image_s_location']);
    $sth->bindParam(":image_link",  $row['image_r_location']);
    $sth->bindParam(":product_url", $row['page_url']);
    $sth->bindParam(":created_at",  $row['date_processed']);
    $sth->bindParam(":description", $row['image_alt']);
    $sth->bindParam(":price",       $row['price']);
    $sth->bindParam(":currency",    $row['currency']);
    $rv  = $sth->execute();
    
    if ($rv === FALSE) {
        printf(STDERR, "Failed to insert: " . $row['page_url'] . "\n");
        return 0;
    }
    return 1;
}

function check_images_and_price($row) {
    if (file_exists($row['image_s_location'])
    && file_exists($row['image_r_location'])
    && isset($row['price']) && $row['price'] > 0.0
    && isset($row['currency']) && $row['currency'] != '') {
        return TRUE;
    }
    return FALSE;
}

function write_last_id($id) {
    file_put_contents(LAST_ID_FILE, $id);
}

function translate_currency($curr) {

    // Returns currency acronym if expressed as a symbol.
    
    $acronym = $curr;
    switch ($curr) {
    
        case '$':
            $acronym = 'USD';
            break;
        case '€':
            $acronym = 'EUR';
            break;
        case '£':
            $acronym = 'GBP';
            break;    
    }
    
    return $acronym;
}

#-----------------------------------------------------------------------------#
$srcdbh = new Database(DBCONN, DBUSER, DBPASS);
$dstdbh = new Database(DEST_DBCONN, DEST_DBUSER, DEST_DBPASS);

// Read each row from the pages table
$last_id = 0;
$count_r = 0;
$count_w = 0;
while (($row = get_row($srcdbh, $last_id)) !== FALSE) {
    $last_id = $row['id'];
    $count_r++;

    if (check_images_and_price($row) === FALSE) {
        continue;
    }

    $count_w += put_row($dstdbh, $row);
    write_last_id($last_id);

    if ($count_w % 100 == 0) {
        printf("Read/Written: %d\t%d\n", $count_r, $count_w);
    }
}

$srcdbh = null;
$dstdbh = null;

printf("Script done, total read/written: %d\t%d.\n", $count_r, $count_w);

exit(0);
?>