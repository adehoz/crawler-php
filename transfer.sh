#!/usr/bin/env bash

MAXOLD=1
if [ -n $1 ]; then
	MAXOLD=$1
fi

for i in `find . -maxdepth 1 \( -name '*.php' -o -name '*.sh' -o -name '*.sql' \) -type f -mtime -${MAXOLD}`; do
	file=${i#./}
	if [ $file = 'clear-domain.sh' ]; then
		echo "Skipping clear-domain.sh"
		continue
	fi
	echo Transferring $file
	scp -i ~/security/amazonnewkey.pem $file ec2-user@jp:/usr/local/classifier/$file
done
