<?php

/* 
 * Class Product enables getting a page from given URL(s), determining product 
 * information such as product image and price and saving product data into the 
 * database table.
 * 
 * The class is shared between scripts crawler.php and pricecheck.php.
 * 
 * Created as a separate file: May 03 2014.
**/

require_once('business_rules.class.php');

class Product
{
    // Properties.
    private $_domain_filter = '';       // process matched domain(s)
    private $_url = '';                 // product url
    private $_image_url = '';           // product image url
    private $_currency = '';            // product price currency
    private $_price = null;             // product price
    private $_sale_currency = '';       // product sale price currency
    private $_sale_price = null;        // product sale price
    private $_content = '';             // product page url
    private $_page_title = '';          // product page title
    private $_url_tokens = array();     // image url tokens
    private $_img_alt_text = '';        // image alt text (to be saved)
    private $_img_mode = '';            // best image determined by (alt|size)
    private $_img_properties = array(); // image width and height

    private $_common = null;            // common class instance
    private $_business_rules = null;    // business rules class instance
    
    private $_dbh = null;               // database handler
    private $_dbdata = array();         // product data from db
    
    private $_thumb_max_height = 150;   // maximum thumbnail height (in px)
    private $_thumb_max_width = 150;    // maximum thumbnail width (in px)
    private $_thumb_quality = 75;       // thumbnail JPEG quality (0 - 100)
    private $_img_quality = 80;         // image JPEG quality (0 - 100)
    private $_thumb_path = '';          // path on the local system for thumb
    private $_img_path = '';            // path on the local system for image
    private $_img_base = '';            // base directory for images.
    
    private $_pricecheck_count = -1;    // url check counter. -1 = unlimited
    private $_pricecheck_rows = array();// temp holder for pricecheck_count recs
    
    private $_currencies_file = 'currencies.txt';
    private $_currencies = array('€', 'EUR');   // default list
    
    private $_threshold_pct = 51;
    private $_threshold_tokens = 2;
    private $_threshold_dimension_px = 260;

    // Constructor.
    public function __construct() {
        $this->_common = new Common();
        $this->_business_rules = new BusinessRules();
        $this->_currencies = $this->_load_currencies();
    }
    
    // Set debug level, -1 for PRODUCTION!
    public function debug($level) {
        $this->_common->set_debug($level);
    }
    
    // Sets database parameters.
    public function set_db($dbconn, $dbuser, $dbpass) {
        $this->_dbh = new Database($dbconn, $dbuser, $dbpass);
    }
    
    // Sets domain filter. Crawler will process only pages matched.
    public function set_domain_filter($filter) {
        $this->_domain_filter = $filter;
    }
    
    // Sets the base directory where images will be saved on a local system.
    public function set_image_basedir($dir) {
        $this->_img_base = $dir;
    }
    
    // Sets the limit on number of URLs to check for a domain.
    public function set_domain_initial_pricechecks($pricechecks) {
        $this->_pricecheck_count = $pricechecks;
    }
    
    // Searches for the first available URL in the database with product
    // URLs. Return the whole record (to keep track of a record id and other
    // interesting details) plus the price tag for the domain from the domains
    // table.
    // TODO: rewrite simpler, move queries to private function.
    public function find_next_url() {
    
        $t0 = microtime(TRUE);
        
        // We need a unique (indexed) field in the database for
        // anti-concurrency purposes.
        $control = md5(getmypid() . time() . rand());
        
        $filter = "";
        if (isset($this->_domain_filter)) {
            $domain_filter = $this->_domain_filter;
            $filter = <<<FILTER
AND sitemap_id IN (
SELECT s.id
FROM sitemaps s, domains d
WHERE s.domain_id = d.id
AND d.domain_url LIKE '%$domain_filter%'
)
FILTER;
        }        
        
        $qu = <<<QUERY
UPDATE pages
SET date_processed = NOW(), control = :control
WHERE date_processed IS NULL
$filter
LIMIT 1
QUERY;
        $this->_common->writeit(3, "FIND NEXT URL UPDATE 1:\n["
            . $qu . "], :control is " . $control . "]");
        
        $this->_dbh->handler->beginTransaction();
        $sth_u = $this->_dbh->handler->prepare($qu);
        $sth_u->bindParam(":control", $control);
        $rv = $sth_u->execute();
        if (FALSE === $rv) {
            $this->_common->writeit(1, 'ERROR on update. QUERY:\n['
                . $qu . '], :control: [' . $control . '], RETURN: ['
                . $sth_u->errorCode() . '][' . $sth_u->errorInfo() . ']');
        }
        $count = $sth_u->rowCount();
        $this->_common->writeit(3, "Rows affected: " . $count);
        $this->_dbh->handler->commit();
        
        $t1 = microtime(TRUE);
        
        $qs = <<<QUERY
SELECT p.*, d.price_tag, d.sale_price_tag, d.image_tag, d.image_callback
FROM pages p, sitemaps s, domains d
WHERE p.control = :control
AND p.sitemap_id = s.id
AND s.domain_id = d.id
QUERY;
        $sth_s = $this->_dbh->handler->prepare($qs);
        $sth_s->bindParam(":control", $control);
        $sth_s->execute();
        $row = $sth_s->fetch(PDO::FETCH_ASSOC);
        $this->_common->writeit(3, "FIND NEXT URL QUERY:\n" . $qs
            . "\n:control is " . $control);
        
        $t2 = microtime(TRUE);
        $this->_common->writeit(2, "BENCH - find_next_url() "
            . "total: " . ($t2 - $t0) . "s, update: " . ($t1 - $t0)
            . "s, select: " . ($t2 - $t1) . "s");

        if (FALSE === $row) {
            return 0;
        }
        $this->_dbdata = $row;

        $qd = <<<QUERY
UPDATE domains
SET price_modified = 0
WHERE id = (SELECT domain_id FROM sitemaps WHERE id = :sitemap_id)
QUERY;
        $sth_d = $this->_dbh->handler->prepare($qd);
        $sth_d->bindParam(":sitemap_id", $row['sitemap_id']);
        $sth_d->execute();
        
        $t3 = microtime(TRUE);
        $this->_common->writeit(2, "BENCH - find_next_url() "
            . "domains table updated in " . ($t3 - $t2));
        
        return 1;
    }
    
    // Test function, allows specifying page to be tested by the page id.
    // TODO: rewrite simpler, move query to private function.
    public function find_url_by_id($id) {
    
        // For test purposes only. Set _dbdata by specifying page id.
        
        $qs = <<<QUERY
SELECT p.*, d.price_tag, d.sale_price_tag, d.image_tag, d.image_callback
FROM pages p, sitemaps s, domains d
WHERE p.id = :id
AND p.sitemap_id = s.id
AND s.domain_id = d.id
QUERY;

        $sth = $this->_dbh->handler->prepare($qs);
        $sth->bindParam(":id", $id);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        $this->_dbdata = $row;
        print_r($this->_dbdata);
        if (FALSE === $row) return 0;
        return 1;
    }
    
    // Set specific _dbdata values for testing.
    public function set_product_data($data) {
        $this->_dbdata = $data;
    }
    
    // Returns rows for price to check. The stored procedure finds the
    // domain not checked for a longest time, and for that domain,
    // a $this->pricecheck_count rows are returned that haven't been checked
    // longest.
    // Thread-safe, next procedure will return different rows,
    // as fields control_pricecheck and date_pricecheck are updated for both
    // domains and pages table.
    public function pricecheck_select($id) {
    
        // If id is passed, select that row only. This is used for testing
        // and control fields are not updated.
        if (isset($id)) {
            $this->_common->writeit(1, "User requested pages id " . $id);
            $q = "SELECT * FROM pages WHERE id = " . $id;
            $sth = $this->_dbh->handler->prepare($q);
            $sth->execute();
        }
    
        // We need a unique (indexed) field in the database for
        // anti-concurrency purposes.
        else {
            $control = md5(getmypid() . time() . rand());
            
            $q = "CALL pricecheck_select(:control, :limit)";
            $sth = $this->_dbh->handler->prepare($q);
            $sth->bindValue(":control", $control, PDO::PARAM_STR);
            $sth->bindValue(":limit", $this->_pricecheck_count, PDO::PARAM_INT);
            $sth->execute();
        }
        
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        if (FALSE === $rows || count($rows) == 0) {
            $this->_common->writeit(1, "No rows returned.");
            return 0;
        }
        
        $this->_pricecheck_rows = $rows;
        print_r($this->_pricecheck_rows);
        return 1;
    }
    
    
    // Loop through the rows obtained in pricecheck_select(), get the content
    // and check whether the price has changed.
    public function pricecheck_loop() {
    
        foreach ($this->_pricecheck_rows as $row) {
            
            // Go to next if page url not defined.
            if (is_null($row['product_url']) || $row['product_url'] == '') {
                continue;
            }
            
            // Get page content.
            $this->_url = $row['product_url'];
            $this->_common->writeit(1, $this->_url);
            $this->get_content();
            
            // Parse URL to get domain name of the product.
            $parse_url = parse_url($this->_url);
            $this->_domain_filter = $parse_url['host'];
            
            // Get price tags from the domains table.
            $q = "SELECT price_tag, sale_price_tag FROM domains"
                . " WHERE domain_url LIKE '%" . $this->_domain_filter . "%'";
            $sth = $this->_dbh->handler->prepare($q);
            $sth->execute();
            $domain_row = $sth->fetch(PDO::FETCH_ASSOC);
            if (FALSE === $domain_row) {
                $this->_common->writeit(1, "No domain like "
                    . $this->_domain_filter);
                continue;
            }
            $this->_dbdata = $domain_row;
            print_r($this->_dbdata);

            $this->_match_price('price_tag');
            $this->_match_price('sale_price_tag');
            
            $this->_pricecheck_update($row);
        }
    }
    
    // Updates regular and sale price, called from pricecheck_loop().
    private function _pricecheck_update($db_row) {
    
        if ($db_row['sale_price'] != $this->_sale_price) {
            $this->_common->writeit(2, "Sale price update [" 
                . $db_row['id'] ."]: "
                . $db_row['sale_price'] . " --> "
                . $this->_sale_price);
            
            $qsp = <<<UPDATE_SALE
UPDATE images SET 
sale_price = :sale_price
WHERE id = :id
UPDATE_SALE;
            $sth = $this->_dbh->handler->prepare($qsp);
            $sth->bindParam(":sale_price", $this->_sale_price);
            $sth->bindParam(":id", $db_row['id']);
            $sth->execute();
        }
        
        if ($db_row['regular_price'] != $this->_price) {
            $this->_common->writeit(2, "Regular price update ["
                . $db_row['id'] ."]: "
                . $db_row['regular_price'] . " --> "
                . $this->_price);

            $qsr = <<<UPDATE_REGULAR
UPDATE images SET 
regular_price = :price,
currency = :currency
WHERE id = :id
UPDATE_REGULAR;
            $sth = $this->_dbh->handler->prepare($qsr);
            $sth->bindParam(":price", $this->_price);
            $sth->bindParam(":currency", $this->_currency);
            $sth->bindParam(":id", $db_row['id']);
            $sth->execute();
        }
    
    }
        
    // Product URL is tokenized as this is the primary way to get the
    // corresponding product image.
    public function set_url_tokens() {
        if (! isset($this->_dbdata['page_url'])) {
            return 0;
        }
        
        $url = $this->_dbdata['page_url'];

        // Tokenize URL. Exit if 'path' component not set (domain url)
        $tokens = parse_url($url);
        if (! isset($tokens['path'])) {
            return 0;
        }
        
        
        // Removed because massimodutti.com uses %26 for '&' in URLs.
        // Do we even need it?
        //$this->_url = urldecode($url);
        $this->_url = $url;
        
        
        $this->_url_tokens = $this->_get_tokens($tokens['path']);
        $this->_url_tokens = array_map('strtolower', $this->_url_tokens);
        
        $this->_common->writeit(0, "PRODUCT URL: " . $url);
        return 1;
    }
    
    // Read the URI content into a string.
    public function get_content() {
        $this->_common->writeit(1, "Accessing page [" . $this->_url . "]");
        $this->_content = file_get_contents($this->_url);
        if ($this->_content === false) {
            echo "Could not get " . $this->_url . "\n";
            return 0;
        }
        return 1;
    }
    
    // Set the content, for test purposes.
    public function set_content($content) {
        $this->_content = $content;
    }
    
    // Save content (debug function)
    public function save_content($filename) {
        $this->_common->writeit(2, "Saving content to " . $filename . "\n");
        file_put_contents($filename, $this->_content);
        return 1;
    }
    
    // Get page title. Will be saved with the image info.
    public function get_page_title() {
        $title = '';
        $content_clean = preg_replace('/[\n]+/', '', $this->_content);
        preg_match('/<\s*title\s*>\s*(.*?)\s*<\s*\/title\s*>/i', 
            $content_clean, $matches);  // a bit paranoid on whitespaces here.
        if (isset($matches[1])) {
            $title = $matches[1];
        }
        $this->_page_title = $title;
        $this->_common->writeit(1, "Page title: " . $title);
    }
   
    
    // If price tag or sale price tag is defined in domains table,
    //try to find the price in the page content.
    public function find_price() {
    
        foreach (array('price_tag', 'sale_price_tag') as $tag) {
            if (isset($this->_dbdata[$tag])) {
                $this->_match_price($tag);
            }
            else {
                $this->_common->writeit(1, $tag . ' not defined, skipping.');
            }
        }
    }
    
    // If image tag is defined in domains table, and price tag or
    // sale price tag is set, try to match image in the page content.
    public function find_image() {
    
        if (isset($this->_dbdata['image_tag']) && (
                   ! is_null($this->_price) || 
                   ! is_null($this->_sale_price))) {
            $this->_match_image();
        }
        else {
            $this->_common->writeit(1, 'image_tag not defined, '
                                    . 'or price not set, skipping.');
        }
    }
    
    
    /******************************************************************
     * PRIVATE FUNCTIONS
     *****************************************************************/
    

    // Returns a list of tokens from a given string.
    // Leave out hostname.
    private function _get_tokens($string) {
        $tokens = preg_split("/[\W\-]+/", $string);
        return $tokens;
    }
    
    // Fixes image link to a get full URL.
    private function _correct_image_link($image_link) {

        // Unescape.
        $image_link = str_replace("\/", "/", $image_link);
        

        $page_url_arr = parse_url($this->_url);
        $image_url_arr = parse_url($image_link);
        $corrected_image_url = $image_link;
        
        // Full url is built from the page hostname.
        if (! isset($image_url_arr['host'])) {
        
            // Absolute path for image.
            if (substr($image_url_arr['path'], 0, 1) == '/') {
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $image_url_arr['path'];
            }
            
            // Relative path for image.
            else {
                // Remove leading dot if exists.
                if (substr($image_url_arr['path'], 0, 1) == '.') {
                    $image_url_arr['path'] = substr($image_url_arr['path'], 1);
                }
                
                $dir = implode('/', explode('/', $page_url_arr['path'], -1));
                if ($dir == '') {
                    $dir = '/';
                }
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $dir . $image_url_arr['path'];
            }        
        }
        
        // Add http:// if scheme not set
        $image_url_arr = parse_url($corrected_image_url);
        if (! isset($image_url_arr['scheme'])) {
        
            $corrected_image_url = $image_link;
            
            // Add http:// if scheme not set.
            $corr_url_arr = parse_url($corrected_image_url);
            if (! isset($corr_url_arr['scheme'])) {
                $corrected_image_url = trim($corrected_image_url, "/");
                $corrected_image_url = "http://" . $corrected_image_url;
            }
        }
        
        return $corrected_image_url;
    }

    // Loads currencies from the file. If file not found, use default list.
    private function _load_currencies() {
        $currencies = file_get_contents($this->_currencies_file);
        if (FALSE === $currencies) {
            return;
        }
        $curr_arr = preg_split("/[\s,]+/", trim($currencies));
        return $curr_arr;
    }
    
    // Gets the node information about the image to be used in price search.
    // Used with pricecheck functions.
    private function _get_image_tag($image_url) {
    
        $node = null;
    
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($this->_content);
        libxml_clear_errors();
        
        $img_tags = $dom->getElementsByTagName('img');
        foreach ($img_tags as $tag) {
            $image_link = $tag->getAttribute('src');
            $corr_image_link = $this->_correct_image_link($image_link);
            
            if ($corr_image_link == $image_url) {
                $node = $tag;
                break;
            }
        }
        
        return $node;
    }

    private function _match_image() {
    
        $subject = preg_replace('/[\r\n]/m', '', $this->_content);
        $pattern = preg_replace('/[\x00-\x1F\x7F]/', 
            '', $this->_dbdata['image_tag']);
        $pattern = preg_quote($pattern, '/');
        if (FALSE !== strpos($pattern, '_C_START')) {
            $pattern = substr_replace($pattern, '(',
                strpos($pattern, '_C_START'), strlen('_C_START'));
        }
        else {
            $pattern = '(' . $pattern;
        }
        if (FALSE !== strpos($pattern, '_C_END')) {
            $pattern = substr_replace($pattern, ')',
                strpos($pattern, '_C_END'), strlen('_C_END'));
        }
        else {
            $pattern .= ')';
        }
        $pattern = str_replace('_C_ANY', '.*?', $pattern);
        $pattern = '/' . $pattern . '/';
        
        $this->_common->writeit(2, "PATTERN: " . $pattern);
        $this->_common->writeit(4, "SUBJECT: " . $subject);
        
        
        preg_match($pattern, $subject, $matches);
        print_r($matches);
        
        $image_url = isset($matches[1]) 
            ? $this->_correct_image_link($matches[1]) : NULL;
        $this->_common->writeit(2, "Image URL: " . $image_url);
        
        // size check
        if (isset($image_url)) {
            $image_size = @getimagesize($image_url);
            if ($image_size[0] < $this->_threshold_dimension_px
            && $image_size[1] < $this->_threshold_dimension_px) {
                $this->_common->writeit(2, "Image too small: [" 
                    . $image_size[0] . ", " . $image_size[1] . "]");
                $image_url = NULL;
            }
        }
        
        $this->_image_url = $image_url;
    }
    
    private function _find_currency($subject) {
    
        // Match currency from the currencies in the given string.
        // Return matched currency.    
    
        foreach ($this->_currencies as $currency) {
            if (FALSE !== strpos($subject, $currency)) {
                $this->_common->writeit(2, "Found currency: " . $currency);
                return $currency;
            }
        }
        
        return NULL;
    }
    
    
    // Check the number returned by _find_value(). We recognize either
    // two decimal places or one decimal place. Otherwise, the number
    // is interpreted as an integer (all commas and points removed).
    private function _price_to_number($price_str) {
    
        $this->_common->writeit(3, "_price_to_number: input = [$price_str]");
    
        // Find position of the decimal point.
        $dec_marks = array(',', '.');
        $dec_position = null;
        foreach (array(-2, -3) as $pos) {
            $char = substr($price_str, $pos, 1);
            if (FALSE === $char) {
                // If the value string length is less than 2,
                // return the float value.
                $price_str = str_replace($dec_marks, '.', $price_str);
                return (floatval($price_str) + 0.00);
            }
            if ($char == '.' || $char == ',') {
                $char = '.';
                $dec_position = $pos;
                $this->_common->writeit(3, "_price_to_number: decimal point "
                    . "at [" . $dec_position . "]");
                break;
            }
        }
        
        // We interpret the number as integer.
        if (is_null($dec_position)) {
            $price_str = str_replace($dec_marks, '', $price_str);
            return (floatval($price_str) + 0.00);
        }

        // Return a proper decimal number (or integer)
        $decimal = substr($price_str, $dec_position + 1);
        $this->_common->writeit(3, "_price_to_number: decimal part ["
            . $decimal . "]");
        $round = substr($price_str, 0, strlen($price_str) + $dec_position);
        $price_str = str_replace($dec_marks, '', $round)
            . "." . $decimal;
        $this->_common->writeit(3, "_price_to_number: output = [$price_str]");
        return (floatval($price_str) + 0.00);
     }
     
    // Find price value in the given string. It may match multiple
    // values. Generally we are interested in the first matched value,
    // but second one may be interpreted as a sale price.
     private function _find_value($subject, $tag) {
        
        // This function may return multiple values, based on preg_match()
        // function. value[1] is the price based on $tag. value[2] is
        // interpreted as sale_price if value[2] defined and sale_price
        // not already defined.
        $find_value = function($p1, $s1) {
            preg_match_all($p1, $s1, $matches);
            print "Testing pattern: $p1\n";
            return $matches;
        };
        
        // Match a 2-point decimal with a decimal dot
        $pattern = '/\b([\d, ]+\.\d{1,2})\b/';
        $value = $find_value($pattern, $subject);
        if (isset($value[1][0])) {
            return $value;
        }
        
        // Match a 2-point decimal with a decimal comma
        $pattern = '/\b([\d\. ]+,\d{1,2})\b/';
        $value = $find_value($pattern, $subject);
        if (isset($value[1][0])) {
            return $value;
        }
        
        // Match an integer.
        $pattern = '/([\d\., ]*\d)/';
        $value = $find_value($pattern, $subject);
        if (isset($value[1][0])) {
            return $value;
        }
        
        return NULL;
    }
    
    // Check the content between regular price and sale price.
    // There should be at least one keyword (tag) that would help
    // us confirm the value we hold as sale price.
    // Returns sale price if keyword found, otherwise NULL.
    private function _verify_sale_price($subject, $price, $sale_price) {
    
        $pos_reg  = strpos($subject, $price);
        $pos_sale = strpos($subject, $sale_price);
        
        if (FALSE === $pos_reg || FALSE === $pos_sale) {
            $this->_common->writeit(1, "Could not verify sale price, "
                . "regular price or sale price not found in the string.");
        }
        
        $check_subject = substr($subject, $pos_reg + strlen($pos_reg), 
                                $pos_sale - $pos_reg - strlen($pos_reg) - 1);
        $keyword_found = $this->_business_rules->search_price_token(
                                $check_subject);
        if (! $keyword_found) {
            return NULL;
        }
        return $sale_price;
    }
    
    // Engine for matching the price in the content.
    // Used for both regular price and sale price.
    // There are business rules regarding sale price.
    private function _match_price($tag) {
    
        $this->_common->writeit(1, "Match price tag: " . $tag
                                . " [" . $this->_dbdata[$tag]. "]");
        
        // Flatten the content.
        $content = preg_replace('/[\r\n]/m',         '', $this->_content);
        $content = preg_replace('/[\x00-\x1F\x7F]/', '', $this->_content);
        //file_put_contents('x.txt', $subject);
    
        // Find position of the tag.
        $pos = strpos($content, $this->_dbdata[$tag]);
        if (FALSE === $pos) {
            $this->_common->writeit(1, "Tag $tag not found.");
            return;
        }
        
        $length = 150;
        if ($tag == 'price_tag' && ! isset($this->_dbdata['sale_price_tag'])) {
            $length = 300;
        }
        
        // Define subject (a part of content to search price in)
        // and search for currency and value.
        $subject = substr($content, $pos, $length);
        $this->_common->writeit(1, "Subject: " . $subject);
        
        $currency = $this->_find_currency($subject);
        if ($currency == NULL) {
            $this->_common->writeit(1, "Currency not found in ["
                . $subject . "]");
        }
        
        $value = $this->_find_value($subject, $tag);
        if (!isset($value[1])) {
            $this->_common->writeit(1, "Value not found in ["
                . $subject . "]");
        }
        
        if ($tag == 'price_tag') {
            $this->_price = $this->_price_to_number($value[1][0]);
            $this->_currency = $currency;
            $this->_common->writeit(3, "Updated internal data: "
                . "tag = price_tag, this->price = " . $this->_price
                . ", this->currency = " . $this->_currency);
            
            if (isset($value[1][1]) && is_null($this->_sale_price)) {
                $sale_price = $this->_price_to_number($value[1][1]);
                $this->_common->writeit(3, "Sale price might be "
                    . $sale_price . ", checking further.");
                if ($this->_business_rules->is_sale_price(
                        $this->_price, $sale_price)) {
                    $this->_sale_price = $sale_price;
                    $this->_common->writeit(3, "Sale price value within "
                        . "business rules, updating internal data: "
                        . "this->sale_price = " . $this->_sale_price);
                }
                else {
                    $this->_common->writeit(3, "Nope.");
                }
                $this->_sale_price = $this->_verify_sale_price(
                        $subject, $value[1][0], $value[1][1]);
            }
        }
        
        if ($tag == 'sale_price_tag') {
            $sale_price = $this->_price_to_number($value[1][0]);
            
            if (is_null($this->_price)) {
                $this->_common->writeit(1, "Sale price found, but I have no"
                    . " regular price. Setting this price to regular price.");
                $this->_price = $sale_price;
                $this->_currency = isset($currency)?$currency:$sale_currency;
                $this->_common->writeit(3, "Updated internal data: "
                    . "tag = price_tag, this->price = " . $this->_price
                    . ", this->currency = " . $this->_currency);
            }
            
            elseif ($this->_business_rules->is_sale_price(
                    $this->_price, $sale_price)) {
                $this->_sale_price = $sale_price;
                $this->_sale_currency = $currency;
                $this->_common->writeit(3, "Sale price value within "
                    . "business rules, updating internal data: "
                    . "this->sale_price = " . $this->_sale_price);
            }
            
            else {
                $this->_common->writeit(3, "Sale price not within business "
                    . "rules, not updating.");
            }
        }
    }
    
    // Saves the original image and the thumbnail image locally.
    // Based on:
    // http://salman-w.blogspot.com/2008/10/ \
    //   resize-images-using-phpgd-library.html
    public function save_image_locally() {
    
        if (! isset($this->_image_url)) {
            return 0;
        }
        
        $corr_image_link = $this->_correct_image_link($this->_image_url);
        
        $image_path = $this->_build_path($this->_image_url, 0);
        if (! isset($image_path)) {
            return 0;
        }
        $thumbnail_image_path = $this->_build_path($this->_image_url, 1);
        if (! isset($thumbnail_image_path)) {
            return 0;
        }

        
        // Get image properties.
        list($source_image_width, $source_image_height, $source_image_type) = 
            getimagesize($corr_image_link);
            
        switch ($source_image_type) {
        
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($corr_image_link);
                break;
                
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($corr_image_link);
                break;
                
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($corr_image_link);
                break;
            
            default:
                $source_gd_image = false;
                $this->_common->writeit(1, "Image format not recognized.");
        }
        
        if ($source_gd_image === false) {
            return 0;
        }
        
        
        // Get original image ratio.
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = 
            $this->_thumb_max_width / $this->_thumb_max_height;
        
        // Resize sides based on the aspect ratio.
        if ($source_image_width <= $this->_thumb_max_width
        && $source_image_height <= $this->_thumb_max_height) {
        
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } 
        
        elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        
            $thumbnail_image_width = 
                (int) ($this->_thumb_max_height * $source_aspect_ratio);
            $thumbnail_image_height = $this->_thumb_max_height;
            
        } 
        
        else {
        
            $thumbnail_image_width = $this->_thumb_max_width;
            $thumbnail_image_height = 
                (int) ($this->_thumb_max_width / $source_aspect_ratio);
        }
        
        // Prepare and save original image.
        $gd_image = imagecreatetruecolor(
            $source_image_width, $source_image_height);
        imagecopy($gd_image, $source_gd_image, 0, 0, 0, 0, 
            $source_image_width, $source_image_height);
        imagejpeg($gd_image, $image_path, $this->_img_quality);
        
        // Prepare and save thumbnail image.
        $thumbnail_gd_image = imagecreatetruecolor(
            $thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, 
            $thumbnail_image_width, $thumbnail_image_height, 
            $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 
            $this->_thumb_quality);
        
        // Delete images from memory.
        imagedestroy($source_gd_image);
        imagedestroy($gd_image);
        imagedestroy($thumbnail_gd_image);
        
        // Save locations
        $this->_img_path = $image_path;
        $this->_thumb_path = $thumbnail_image_path;
    
        return 1;
    }
    
    // Creates a destination location for a given image.
    private function _build_path($url, $mode) {
    
        // Check if base dir exists.
        if (@is_dir($this->_img_base) === FALSE) {
            fwrite(STDERR, "ERROR: Not a directory: " . $this->_img_base);
            return null;
        }
        
        // If URL not set, return.
        if ($url == "") {
            $this->_common->writeit(2, "Image not set.");
            return null;
        }
        
        // Build image filename.
        $suffix = "";
        if ($mode == 1) {
            $suffix = "_t";
        }
        $filename = md5($url) . $suffix . ".jpg";
        
        // First subdir.
        $url_tokens = parse_url($url);
        $subdir_1 = $url_tokens['host'];
        $dir_1 = $this->_img_base . '/' . $subdir_1;
        if (@is_dir($dir_1) === FALSE) {
            $status = (mkdir($dir_1, 0755));
            if ($status === FALSE) {
                fwrite(STDERR, "ERROR: Failed to create: " . $dir_1);
                return null;
            }
        }
        
        // Second subdir (max 1000 subdirectories).
        $subdir_2 = substr($filename, 0, 3);
        $dir_2 = $dir_1 . '/' . $subdir_2;
        if (@is_dir($dir_2) === FALSE) {
            $status = (mkdir($dir_2, 0755));
            if ($status === FALSE) {
                fwrite(STDERR, "ERROR: Failed to create: " . $dir_2);
                return null;
            }
        }
        
        $filepath = $dir_2 . '/' . $filename;
        $this->_common->writeit(2, "Image filepath: " . $filepath);
        return $filepath;
    }
    
    // Stores image data (product URL, image URL) to the database.
    public function store_image_and_price_data() {
        
        $md5page = md5($this->_url);
        $col_page_url_md5 = "page_url_md5";
        
        try {
            $q = <<<QUERY
UPDATE pages SET
page_title = :page_title,
image_url = :image_url,
image_url_md5 = MD5(image_url),
image_alt = :image_alt,
image_mode = :image_mode,
image_r_location = :image_r_location,
image_s_location = :image_s_location,
image_width = :image_width,
image_height = :image_height,
price = :price,
sale_price = :sale_price,
currency = :currency
WHERE $col_page_url_md5 = :md5page
QUERY;
            $ins = $this->_dbh->handler->prepare($q);
            $ins->bindParam(":md5page", $md5page);
            $ins->bindParam(":page_title", $this->_page_title);
            $ins->bindParam(":image_url", $this->_image_url);
            $ins->bindParam(":image_alt", $this->_img_alt_text);
            $ins->bindParam(":image_mode", $this->_img_mode);
            $ins->bindParam(":image_r_location", $this->_img_path);
            $ins->bindParam(":image_s_location", $this->_thumb_path);
            $ins->bindParam(":image_width", $this->_img_properties[0]);
            $ins->bindParam(":image_height", $this->_img_properties[1]);
            $ins->bindParam(":price", $this->_price);
            $ins->bindParam(":sale_price", $this->_sale_price);
            $ins->bindParam(":currency", $this->_currency);
            $ins->execute();
        }
        catch (PDOException $e) {
            echo "Error saving image and price data: " . $e->getMessage() . "("
                . $e->getCode() . ")\n";
            return 0;
        }
        
        return 1;
    }
}


?>