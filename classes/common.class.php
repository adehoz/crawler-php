<?php

class Database {

    public $handler = null;
    
    // Create database connection.
    function __construct($dbconn, $dbuser, $dbpass) {
    
        try {
            $this->handler = new PDO($dbconn, $dbuser, $dbpass);
            $this->handler->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $this->handler->setAttribute(PDO::ATTR_ERRMODE, 
                PDO::ERRMODE_WARNING);
        }
        catch (PDOException $e) {
            fwrite(STDERR, "Error opening database: " . $e->getMessage() . "(" 
                . $e->getCode() . ")\n");
        }
    }
    
    // Disconnect from database.
    function __destruct() {
        $this->handler = null;
    }
    
    // Fetch row from pages table (or phpcrawler table)
    public function get_product_data($table, $date_column) {
    
        // We need a unique (indexed) field in the database for
        // anti-concurrency purposes.
        $control = md5(getmypid() . time() . rand());

        try {

            $qu = <<<QUERY
UPDATE $table
SET $date_column = NOW(), control = :control
WHERE $date_column IS NULL
LIMIT 1
QUERY;
            $sth_u = $this->handler->prepare($qu);
            $sth_u->bindParam(":control", $control);
            $sth_u->execute();

            $qs = <<<QUERY
SELECT *
FROM $table
WHERE control = :control
QUERY;
            $sth_s = $this->handler->prepare($qs);
            $sth_s->bindParam(":control", $control);
            $sth_s->execute();
            $row = $sth_s->fetch(PDO::FETCH_ASSOC);
        }

        catch (PDOException $e) {
            fwrite(STDERR, "DATABASE ERROR: " . $e->getMessage()
                . " [" . $e->getCode() . "]\n");
            return NULL;
        }

        if (FALSE === $row) {
            return NULL;
        }
        return $row;
    }
}

class Common {

    // Properties.
    private $_debug = -1;       // Debug level. -1 for PROD, >= 0 for debug.

    // Constructor does nothing.
    function __construct() {}
    
    // Set debug level. Will be -1 (production value) if not explicitly set,
    // and no debug messages will be printed out.
    public function set_debug($level) {
        $this->_debug = $level;
        print "LOG LEVEL: " . $this->_debug . "\n";
    }
    
    // Debug output.
    public function writeit($level, $message) {
        if ($level <= $this->_debug) {
            $date = date('Y-m-d H:i:s');
            print "[D" . $level . "][" . $date . "] " . $message . "\n";
        }
        //print "[X:" . $level . ":" . $this->_debug . "] " . $message . "\n";
    }

    // Returns a list of tokens from a given string.
    // Leave out hostname.
    public function tokens($mode, $string) {
    
        $tokens = array();
        
        if ($mode == 'url') {
            $url_tokens = parse_url($string);
            if (! isset($url_tokens['path'])) {
                return array();
            }
            $string = $url_tokens['path'];
        }
    
        $tokens = preg_split("/[\W\-]+/", $string);
        $tokens = $this->_check_special_tokens($tokens);
        return $tokens;
    }
    
    // Some tokens, normally separated by a dash, are split into
    // tokens which change meanings or have no meanings. Check for them
    // and fix them. Example: t-shirt.
    private function _check_special_tokens($tokens) {
    
        $specials = array(
        
            't-shirt',
            'one-piece',
            'light-pink',
        );
        
        foreach ($specials as $special) {
        
            $splitted = explode('-', $special);
            
            for ($i = 0; $i < count($tokens) - 1; $i++) {
            
                if ($tokens[$i] == $splitted[0]
                && $tokens[$i+1] == $splitted[1]) {
                
                    $tokens[$i] = $special;
                    $tokens[$i+1] = '';
                }
            }
        }
    
        return $tokens;
    }
    
}

?>