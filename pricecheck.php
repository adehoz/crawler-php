<?php

/******************************************************************************
 * Program: Price Checker
 * Author:  Domagoj Marsic <domagoj.marsic@gmail.com>
 * Version: 1.0
 * Created: May 3 2014
 * Updated: ---
 *
 * For product pages, GETs the page from the Internet and checks whether
 * the price was changed.
 *
*****************************************************************************/

require_once('config.inc.php');
require_once('common.inc.php');
require_once('product.class.php');

$opts = getopt("i:");
$userdefined_id = isset($opts['i']) ? $opts['i'] : null;

while (1) {
    $product = new Product();
    $product->debug(DEBUG_LEVEL);
    $product->set_db(DBCONN, DBUSER, DBPASS);
    $product->set_domain_initial_pricechecks(DOMAIN_PRICECHECKS);
    if (! $product->pricecheck_select($userdefined_id)) {
        break;
    }
    $product->pricecheck_loop();
    
    if (isset($userdefined_id)) {
        break;
    }
}
