<?php

/* 
 * Class Product enables getting a page from given URL(s), determining product 
 * information such as product image and price and saving product data into the 
 * database table.
 * 
 * The class is shared between scripts crawler.php and pricecheck.php.
 * 
 * Created as a separate file: May 03 2014.
**/

class Product
{
    // Properties.
    private $_mode = '';                // image obtained by {sitemap|crawler}
    private $_domain_filter = '';       // process matched domain(s)
    private $_url = '';                 // product url
    private $_image_url = '';           // product image url
    private $_currency = '';            // product price currency
    private $_price = null;             // product price
    private $_content = '';             // product page url
    private $_page_title = '';          // product page title
    private $_url_tokens = array();     // image url tokens
    private $_img_alt_text = '';        // image alt text (to be saved)
    private $_img_mode = '';            // best image determined by (alt|size)
    private $_img_properties = array(); // image width and height

    private $_common = null;            // common class instance
    
    private $_dbh = null;               // database handler
    private $_dbdata = array();         // product data from db
    
    private $_thumb_max_height = 150;   // maximum thumbnail height (in px)
    private $_thumb_max_width = 150;    // maximum thumbnail width (in px)
    private $_thumb_quality = 75;       // thumbnail JPEG quality (0 - 100)
    private $_img_quality = 80;         // image JPEG quality (0 - 100)
    private $_thumb_path = '';          // path on the local system for thumb
    private $_img_path = '';            // path on the local system for image
    private $_img_base = '';            // base directory for images.
    
    private $_pricecheck_domain = null; // pricecheck domain id
    private $_pricecheck_count = -1;    // url check counter. -1 = unlimited
    private $_pricecheck_rows = array();// temp holder for pricecheck_count recs
    
    private $_currencies_file = 'currencies.txt';
    private $_currencies = array('€', 'EUR');   // default list
    
    private $_img_decision = array(
        'ALT'   => 0,
        'SIZE'  => 1,
        'CRITERIAFAIL' => 9
    );
    
    private $_threshold_pct = 51;
    private $_threshold_tokens = 2;
    private $_threshold_dimension_px = 300;

    // Constructor.
    public function __construct($mode) {
        $this->_mode = $mode;
        $this->_common = new Common();
        $this->_currencies = $this->_load_currencies();
        $this->_common->writeit(2, "Currencies: "
            . join('|', $this->_currencies));
    }
    
    // Returns a list of tokens from a given string.
    // Leave out hostname.
    private function _get_tokens($string) {
        $tokens = preg_split("/[\W\-]+/", $string);
        return $tokens;
    }
    
    // Matches two arrays (tokens from url and tokens from image alt attr).
    // Returns number in first, second array and a number of matches.
    private function _match_tokens($url_tokens, $alt_tokens) {
        
        // delete empty strings.
        $empty = "";
        if (($key = array_search($empty, $url_tokens)) !== false) {
            unset($url_tokens[$key]);
        }
        if (($key = array_search($empty, $alt_tokens)) !== false) {
            unset($alt_tokens[$key]);
        }
    
        // get match of two arrays.
        $match_arr = array_intersect($url_tokens, $alt_tokens);
        return array(
            'match' => count($match_arr),
            'url_tokens' => count($url_tokens),
            'alt_tokens' => count($alt_tokens)
        );
    }
    
    // Fixes image link to a get full URL.
    private function _correct_image_link($image_link) {

        // Unescape.
        $image_link = str_replace("\/", "/", $image_link);
        

        $page_url_arr = parse_url($this->_url);
        $image_url_arr = parse_url($image_link);
        $corrected_image_url = $image_link;
        
        // Full url is built from the page hostname.
        if (! isset($image_url_arr['host'])) {
        
            // Absolute path for image.
            if (substr($image_url_arr['path'], 0, 1) == '/') {
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $image_url_arr['path'];
            }
            
            // Relative path for image.
            else {
                // Remove leading dot if exists.
                if (substr($image_url_arr['path'], 0, 1) == '.') {
                    $image_url_arr['path'] = substr($image_url_arr['path'], 1);
                }
                
                $dir = implode('/', explode('/', $page_url_arr['path'], -1));
                if ($dir == '') {
                    $dir = '/';
                }
                $corrected_image_url = $page_url_arr['scheme'] . '://'
                    . $page_url_arr['host'] . $dir . $image_url_arr['path'];
            }        
        }
        
        // Add http:// if scheme not set
        $image_url_arr = parse_url($corrected_image_url);
        if (! isset($image_url_arr['scheme'])) {
        
            $corrected_image_url = $image_link;
            
            // Add http:// if scheme not set.
            $corr_url_arr = parse_url($corrected_image_url);
            if (! isset($corr_url_arr['scheme'])) {
                $corrected_image_url = trim($corrected_image_url, "/");
                $corrected_image_url = "http://" . $corrected_image_url;
            }
        }
        
        return $corrected_image_url;
    }

    // Set debug level, -1 for PRODUCTION!
    public function debug($level) {
        $this->_common->set_debug($level);
    }
    
    // Sets database parameters.
    public function set_db($dbconn, $dbuser, $dbpass) {
        $this->_dbh = new Database($dbconn, $dbuser, $dbpass);
    }
    
    // Sets domain filter. Crawler will process only pages matched.
    public function set_domain_filter($filter) {
        $this->_domain_filter = $filter;
    }
    
    // Sets the base directory where images will be saved on a local system.
    public function set_image_basedir($dir) {
        $this->_img_base = $dir;
    }
    
    // Sets the limit on number of URLs to check for a domain.
    public function set_domain_initial_pricechecks($pricechecks) {
        $this->_pricecheck_count = $pricechecks;
    }
    
    // Loads currencies from the file. If file not found, use default list.
    private function _load_currencies() {
        $currencies = file_get_contents($this->_currencies_file);
        if (FALSE === $currencies) {
            return;
        }
        $curr_arr = preg_split("/[\s,]+/", trim($currencies));
        return $curr_arr;
    }
    
    // Searches for the first available URL in the database with product
    // URLs. Return the whole record (to keep track of a record id and other
    // interesting details) plus the price tag for the domain from the domains
    // table. 
    public function find_next_url() {
    
        $t0 = microtime(TRUE);
        
        // We need a unique (indexed) field in the database for
        // anti-concurrency purposes.
        $control = md5(getmypid() . time() . rand());
        
        $tbl_page_links = TBL_PAGE_LINKS;
        $tbl_sitemaps = TBL_SITEMAPS;
        $tbl_domains = TBL_DOMAINS;

        $filter = "";
        if (isset($this->_domain_filter)) {
            $domain_filter = $this->_domain_filter;
            $filter = <<<FILTER
AND sitemap_id IN (
SELECT s.id
FROM $tbl_sitemaps s, $tbl_domains d
WHERE s.domain_id = d.id
AND d.domain_url LIKE '%$domain_filter%'
)
FILTER;
        }        
        
        $qu = <<<QUERY
UPDATE $tbl_page_links
SET date_processed = NOW(), control = :control
WHERE date_processed IS NULL
$filter
LIMIT 1
QUERY;
        $this->_common->writeit(3, "FIND NEXT URL UPDATE 1:\n["
            . $qu . "], :control is " . $control . "]");
        
        $this->_dbh->handler->beginTransaction();
        $sth_u = $this->_dbh->handler->prepare($qu);
        $sth_u->bindParam(":control", $control);
        $rv = $sth_u->execute();
        if (FALSE === $rv) {
            $this->_common->writeit(1, 'ERROR on update. QUERY:\n['
                . $qu . '], :control: [' . $control . '], RETURN: ['
                . $sth_u->errorCode() . '][' . $sth_u->errorInfo() . ']');
        }
        $count = $sth_u->rowCount();
        $this->_common->writeit(3, "Rows affected: " . $count);
        $this->_dbh->handler->commit();
        
        $t1 = microtime(TRUE);
        
        $qs = <<<QUERY
SELECT p.*, d.price_tag, d.image_tag, d.image_callback
FROM $tbl_page_links p, $tbl_sitemaps s, $tbl_domains d
WHERE p.control = :control
AND p.sitemap_id = s.id
AND s.domain_id = d.id
QUERY;
        $sth_s = $this->_dbh->handler->prepare($qs);
        $sth_s->bindParam(":control", $control);
        $sth_s->execute();
        $row = $sth_s->fetch(PDO::FETCH_ASSOC);
        $this->_common->writeit(3, "FIND NEXT URL QUERY:\n" . $qs
            . "\n:control is " . $control);
        
        $t2 = microtime(TRUE);
        $this->_common->writeit(2, "BENCH - find_next_url() "
            . "total: " . ($t2 - $t0) . "s, update: " . ($t1 - $t0)
            . "s, select: " . ($t2 - $t1) . "s");

        if (FALSE === $row) {
            return 0;
        }
        $this->_dbdata = $row;

        $qd = <<<QUERY
UPDATE $tbl_domains
SET price_modified = 0
WHERE id = (SELECT domain_id FROM $tbl_sitemaps WHERE id = :sitemap_id)
QUERY;
        $sth_d = $this->_dbh->handler->prepare($qd);
        $sth_d->bindParam(":sitemap_id", $row['sitemap_id']);
        $sth_d->execute();
        
        $t3 = microtime(TRUE);
        $this->_common->writeit(2, "BENCH - find_next_url() "
            . "domains table updated in " . ($t3 - $t2));
        
        return 1;
    }
    
    public function find_url_by_id($id) {
    
        // For test purposes only. Set _dbdata by specifying page id.
        
        $tbl_page_links = TBL_PAGE_LINKS;
        $tbl_sitemaps = TBL_SITEMAPS;
        $tbl_domains = TBL_DOMAINS;

        $qs = <<<QUERY
SELECT p.*, d.price_tag
FROM $tbl_page_links p, $tbl_sitemaps s, $tbl_domains d
WHERE p.id = :id
AND p.sitemap_id = s.id
AND s.domain_id = d.id
QUERY;

        $sth = $this->_dbh->handler->prepare($qs);
        $sth->bindParam(":id", $id);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        $this->_dbdata = $row;
        print_r($this->_dbdata);
        if (FALSE === $row) return 0;
        return 1;
    }
    
    // Returns rows for price to check. The stored procedure finds the
    // domain not checked for a longest time, and for that domain,
    // a $this->pricecheck_count rows are returned that haven't been checked
    // longest.
    // Thread-safe, next procedure will return different rows,
    // as fields control_pricecheck and date_pricecheck are updated for both
    // domains and pages table.
    public function pricecheck_select($id) {
    
        // If id is passed, select that row only. This is used for testing
        // and control fields are not updated.
        if (isset($id)) {
            $this->_common->writeit(1, "User requested pages id " . $id);
            $q = "SELECT * FROM " . TBL_PAGE_LINKS . " WHERE id = " . $id;
            $sth = $this->_dbh->handler->prepare($q);
            $sth->execute();
        }
    
        // We need a unique (indexed) field in the database for
        // anti-concurrency purposes.
        else {
            $control = md5(getmypid() . time() . rand());
            
            $q = "CALL pricecheck_select(:control, :limit)";
            $sth = $this->_dbh->handler->prepare($q);
            $sth->bindValue(":control", $control, PDO::PARAM_STR);
            $sth->bindValue(":limit", $this->_pricecheck_count, PDO::PARAM_INT);
            $sth->execute();
        }
        
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        if (FALSE === $rows || count($rows) == 0) {
            $this->_common->writeit(1, "No rows returned.");
            return 0;
        }
        
        $this->_pricecheck_rows = $rows;
        return 1;
    }
    
    
    // Loop through the rows obtained in pricecheck_select(), get the content
    // and check whether the price has changed.
    public function pricecheck_loop() {
    
        // Set to 1 as soon as price change detected in this domain.
        $f_end_domain_check = 0;
    
        foreach ($this->_pricecheck_rows as $row) {
            
            // Go to next if page url not defined.
            if (is_null($row['page_url']) || $row['page_url'] == '') {
                continue;
            }
            
            // Get page contents.
            $this->_url = $row['page_url'];
            $this->_common->writeit(2, $this->_url);
            $this->get_content();
            
            // If image url already defined, then just find its node.
            // Otherwise find best matching image and price.
            if (isset($row['image_url']) && $row['image_url'] != '') {
                $node = $this->_get_image_tag($row['image_url']);
                $this->_get_price($node);
            }
            else {
                // If this script should search for image and price even
                // it has not been previously defined, uncomment the row below:
                //$this->get_product_image_and_price();
            }
            
            if ($row['price'] != $this->_price
            || $row['currency'] != $this->_currency) {
                $this->_common->writeit(2, "Price changed, old: "
                    . $row['currency'] . $row['price']
                    . ". Update needed.");
                $this->_pricecheck_update_needed($row);
                $f_end_domain_check = 1;
            }
            
            // Cleanup.
            $this->_url = null;
            $this->_image_url = null;
            $this->_price = null;
            $this->_currency = null;
            
            if ($f_end_domain_check) {
                break;
            }
        }
    }
    
    // Update domains table's price modified field to indicate that there's
    // a price change for this domain and that all domain needs to be rerun.
    private function _pricecheck_update_needed($row) {
    
        $tbl_domains = TBL_DOMAINS;
        $tbl_sitemaps = TBL_SITEMAPS;
        $tbl_page_links = TBL_PAGE_LINKS;
    
        try {
            $q = "SELECT domain_id FROM $tbl_sitemaps WHERE id = :sitemap_id";
            $sth_q = $this->_dbh->handler->prepare($q);
            $sth_q->bindParam(":sitemap_id", $row['sitemap_id']);
            $sth_q->execute();
            $row = $sth_q->fetch(PDO::FETCH_ASSOC);
            $domain_id = $row['domain_id'];
            
            $this->_common->writeit(2, "Update date_processed and control "
                . "for pages with domain_id: " . $domain_id);
            
            $qu = <<<QUERY
UPDATE $tbl_domains
SET price_modified = 1
WHERE id = :domain_id
QUERY;

            $sth = $this->_dbh->handler->prepare($qu);
            $sth->bindParam(":domain_id", $domain_id);
            $sth->execute();
            
            $qn = <<<UPDATE_NULL
UPDATE $tbl_page_links
SET date_processed = NULL, control = NULL
WHERE sitemap_id IN (
    SELECT id
    FROM $tbl_sitemaps
    WHERE domain_id = :domain_id
)
UPDATE_NULL;
            $sth_qn = $this->_dbh->handler->prepare($qn);
            $sth_qn->bindParam(":domain_id", $domain_id);
            $sth_qn->execute();

        }

        catch (PDOException $e) {
            echo "Error saving image: " . $e->getMessage() . "("
                . $e->getCode() . ")\n";
            return 0;
        }
        
        return 1;
    }
    
    // Product URL is tokenized as this is the primary way to get the
    // corresponding product image.
    public function set_url_tokens() {
        if (! isset($this->_dbdata['page_url'])) {
            return 0;
        }
        
        $url = $this->_dbdata['page_url'];

        // Tokenize URL. Exit if 'path' component not set (domain url)
        $tokens = parse_url($url);
        if (! isset($tokens['path'])) {
            return 0;
        }
        
        $this->_url = urldecode($url);
        $this->_url_tokens = $this->_get_tokens($tokens['path']);
        $this->_url_tokens = array_map('strtolower', $this->_url_tokens);
        
        $this->_common->writeit(0, "PRODUCT URL: " . $url);
        return 1;
    }
    
    // Read the URI content into a string.
    public function get_content() {
        $this->_common->writeit(1, "Accessing page [" . $this->_url . "]");
        $this->_content = file_get_contents($this->_url);
        if ($this->_content === false) {
            echo "Could not get " . $this->_url . "\n";
            return 0;
        }
        return 1;
    }
    
    // Save content (debug function)
    public function save_content($filename) {
        $this->_common->writeit(2, "Saving content to " . $filename . "\n");
        file_put_contents($filename, $this->_content);
        return 1;
    }
    
    // Get page title. Will be saved with the image info.
    public function get_page_title() {
        $title = '';
        $content_clean = preg_replace('/[\n]+/', '', $this->_content);
        preg_match('/<\s*title\s*>\s*(.*?)\s*<\s*\/title\s*>/i', 
            $content_clean, $matches);  // a bit paranoid on whitespaces here.
        if (isset($matches[1])) {
            $title = $matches[1];
        }
        $this->_page_title = $title;
        $this->_common->writeit(1, "Page title: " . $title);
    }
   

    // Gets the node information about the image to be used in price search.
    // Used with pricecheck functions.
    private function _get_image_tag($image_url) {
    
        $node = null;
    
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($this->_content);
        libxml_clear_errors();
        
        $img_tags = $dom->getElementsByTagName('img');
        foreach ($img_tags as $tag) {
            $image_link = $tag->getAttribute('src');
            $corr_image_link = $this->_correct_image_link($image_link);
            
            if ($corr_image_link == $image_url) {
                $node = $tag;
                break;
            }
        }
        
        return $node;
    }
    
    // Find all images in the URL content, then find the correct one for
    // the product based on the criteria.
    // Criteria:
    // 1. URL token / alt token match
    // 2. image size
    public function get_product_image_and_price() {
    
        // If price tag is set and it can be found in the content, this is a
        // product page and find the image; otherwise, it's not a content and
        // skip.
        // If price tag is not set, then try with best effort to find the
        // product image and the corresponding price.
        if (isset($this->_dbdata['price_tag'])) {
            $price = $this->_match_price();
            $this->_match_image();
            /*if (is_null($price)) {
                $this->_product_not_found();
            }
            else {
                $this->_search_image();
            }*/
        }
        
        else {
            $node = $this->_search_image();
            $this->_search_price($node);
        }
        
        return;
    }
    
    public function _search_image() {
    
        // Use DOM to parse and search for images.
        // libxml_use_internal_errors() used to disable passing of errors
        // and warnings from libxml to PHP.
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($this->_content);
        libxml_clear_errors();
        
        $img_tags = $dom->getElementsByTagName('img');
        $largest_image_size = 0;
        $largest_image_properties_size = array();
        $same_image_size_size_counter = 0;
        $same_image_size_token_counter = 0;

        $best_token_match = array(
            'match' => 0,
            'url_tokens' => 0,
            'alt_tokens' => 0,
            'size' => 0,
            'properties' => array(0, 0)
        );

        $image_url_size = '';
        $image_tag_size = null;

        // Go through each image to select the best image.
        foreach ($img_tags as $tag) {
        
            // Get the correctly formed image link.
            $image_link = $tag->getAttribute('src');
            $corr_image_link = $this->_correct_image_link($image_link);

            // Find image style. If display: none, safely skip.
            $image_style = $tag->getAttribute('style');
            if (preg_match("/display\s*:\s*none/i", $image_style)) {
                continue;
            }

            // Determine image size.
            $image_size = @getimagesize($corr_image_link);
            $size_b = $image_size[0] * $image_size[1];
            $this->_common->writeit(3, "IMG [" . $size_b . "] " 
                . $corr_image_link);

            // Found new largest image.
            if ($size_b > $largest_image_size) {
                $largest_image_size = $size_b;
                $largest_image_properties_size = $image_size;
                $same_image_size_size_counter = 1;
                $image_url_size = $corr_image_link;
                $image_tag_size = $tag;
            }
            // If there's an image of the same size, keep track.
            // That might be a product category with many pictures of the
            // same size. We will set threshold to 1.
            elseif ($size_b == $largest_image_size) {
                $same_image_size_size_counter++;
            }
            
            // Keep track of the same size images of our best token fit.
            if ($size_b == $best_token_match['size']) {
                $same_image_size_token_counter++;
            }
           
            // We need the image tag for find the price in proximity.
            $image_tag = $tag;
        }
        
        // Size criteria.
        $this->_img_properties = $largest_image_properties_size;
        $status_s = $this->_size_criteria(
            $largest_image_properties_size, $same_image_size_size_counter);

        if ($status_s == 0) {
            $this->_image_url = $image_url_size;
            $this->_img_mode = $this->_img_decision['SIZE'];

            print "\tProduct URL:   " . $this->_url . "\n";
            print "\tImage URL:     " . $this->_image_url . "\n";
            return $image_tag;
        }
        
        // If we didn't find the image.
        $this->_product_not_found();
        return NULL;
    }
    
    private function _price_to_number($price_str) {
    
        // Check if decimal number
        $char = substr($price_str, -3, 1);
        $dec_marks = array(',', '.');
        
        // The string is too short, just return the value
        if (FALSE === $char) {
            $price_str = str_replace($dec_marks, '', $price_str);
            return (floatval($price_str) + 0.00);
        }
        
        // If it is decimal, then convert
        if ($char == '.' || $char == ',') {
            $decimal = substr($price_str, -2);
            $round = substr($price_str, 0, strlen($price_str) - 3);
            $price_str = str_replace($dec_marks, '', $round)
                . "." . $decimal;
            return (floatval($price_str) + 0.00);
        }
        
        // If not decimal, return the value.
        $price_str = str_replace($dec_marks, '', $price_str);
        return (floatval($price_str) + 0.00);
     }
    
    private function _match_image() {
    
        $subject = preg_replace('/[\r\n]/m', '', $this->_content);
        $pattern = preg_replace('/[\x00-\x1F\x7F]/', 
            '', $this->_dbdata['image_tag']);
        $pattern = preg_quote($pattern, '/');
        if (FALSE !== strpos($pattern, '_C_START')) {
            $pattern = substr_replace($pattern, '(',
                strpos($pattern, '_C_START'), strlen('_C_START'));
        }
        else {
            $pattern = '(' . $pattern;
        }
        if (FALSE !== strpos($pattern, '_C_END')) {
            $pattern = substr_replace($pattern, ')',
                strpos($pattern, '_C_END'), strlen('_C_END'));
        }
        else {
            $pattern .= ')';
        }
        $pattern = str_replace('_C_ANY', '.*?', $pattern);
        $pattern = '/' . $pattern . '/';
        
        $this->_common->writeit(2, "PATTERN: " . $pattern);
        $this->_common->writeit(4, "SUBJECT: " . $subject);
        
        
        preg_match($pattern, $subject, $matches);
        print_r($matches);
        
        $image_url = isset($matches[1]) 
            ? $this->_correct_image_link($matches[1]) : NULL;
        $this->_common->writeit(2, "Image URL: " . $image_url);
        
        // size check
        if (isset($image_url)) {
            $image_size = @getimagesize($image_url);
            if ($image_size[0] < $this->_threshold_dimension_px
            && $image_size[1] < $this->_threshold_dimension_px) {
                $this->_common->writeit(2, "Image too small: [" 
                    . $image_size[0] . ", " . $image_size[1] . "]");
                $image_url = NULL;
            }
        }
        
        $this->_image_url = $image_url;
    }
    
    private function _match_price() {
    
        // Based on the page content, try to match price tag (read from the
        // domains table) to extract the price and the currency.
        // Return array(price, currency) if matched.
        // Return NULL if not matched.
    
    
        // Prepare subject
        $subject = preg_replace('/[\r\n]/m', '', $this->_content);
        //$this->_common->writeit(2, "CONTENT:\n--------\n" 
        //    . $subject . "\n---------\n");
    
        // Prepare pattern.
        
        // Remove all control characters.
        $pattern = preg_replace('/[\x00-\x1F\x7F]/', 
            '', $this->_dbdata['price_tag']);
        // Escape special characters.
        $pattern = preg_quote($pattern, '/');
        // Replace custom variables with the named capture groups
        
        if (FALSE !== strpos($pattern, '_C_CURRENCY')) {
            $pattern = substr_replace($pattern, '(?P<currency>.*?)',
                strpos($pattern, '_C_CURRENCY'), strlen('_C_CURRENCY'));
        }
        else {
            $currency = $this->_currencies[0];
            print "Setting currency to " . $currency . "\n";
        }
        
        if (FALSE !== strpos($pattern, '_C_VALUE')) {
            $pattern = substr_replace($pattern, '(?P<value>[\d,\. ]*?\d)',
                strpos($pattern, '_C_VALUE'), strlen('_C_VALUE'));
            $f_c_value = 1;     // _c_value set in pattern
        }
        else {
            $f_c_value = 0;     // _c_value not set in pattern
        }
        
        if (FALSE !== strpos($pattern, '_C_NLBS')) {
            $pattern = substr_replace($pattern, '(?<!',
                strpos($pattern, '_C_NLBS'), strlen('_C_NLBS'));
        }
        
        if (FALSE !== strpos($pattern, '_C_NLBE')) {
            $pattern = substr_replace($pattern, ')', 
                strpos($pattern, '_C_NLBE'), strlen('_C_NLBE'));
        }
        
        // Replace custom _any_ with the actual regex expression
        $pattern = str_replace('_C_ANY', '.*?', $pattern);
        $pattern = str_replace('_C_WS', '\s+', $pattern);
        // Add slashes around the pattern.
        $pattern = '/' . $pattern . '/';
        $this->_common->writeit(2, "PATTERN: " . $pattern);
        $this->_common->writeit(4, "SUBJECT: " . $subject);
        
        
        preg_match($pattern, $subject, $matches);
        print_r($matches);
        $value = isset($matches['value']) ? $matches['value'] : null;
        $currency = 
            isset($matches['currency']) ? $matches['currency'] : $currency;
        
        // if _c_value not set in pattern, search for an integer or a decimal
        // valid: 12 12,500 12.99 12,500.99 125000.99
        if ($f_c_value == 0) {
            preg_match($pattern, $subject, $matches);
            if (isset($matches[0])) {
                $extract = $matches[0];
                preg_match("/([\d\.,]+)([\.,]\d{2})/", $extract, $matches1);
                if (isset($matches1[1])) {
                    $value = $this->_price_to_number($matches1[1]);
                }
                if (isset($matches1[2])) {
                    $value += $matches1[2];
                }
            }
        }
        
        if (! isset($value) || ! isset($currency)) {
            print "Price tag --> no match.\n";
            return NULL;
        }
        
        $price = array(
            'value' => $value,
            'currency' => $currency
        );
        $price['value'] = preg_replace('/\D\./', '', $price['value']);
        
        $this->_price = $price['value'];
        $this->_currency = trim($price['currency']);
        $this->_common->writeit(2, "Price tag matched: [" .
            $price['value'] . "][" . $price['currency'] . "]");
        
        
        return $price;
    }
    
    private function _product_not_found() {
    
        // Product image not determined, reset values.
        
        $this->_image_url = '';
        $this->_img_mode = $this->_img_decision['CRITERIAFAIL'];
        //$this->_price = null;
        //$this->_currency = '';
        
        $this->_common->writeit(1, "Image not found. Variables cleared.");
    }
    
    // Checks if we have a token match.
    private function _token_match_criteria(
        $best_token_match,
        $image_url,
        $same_image_size_counter) {
    
        if ($same_image_size_counter > 5) {
            print "\tMultiple images have the same size as image selected "
                . "by token match [" . $same_image_size_counter . "]. "
                . "Possibly category page, ignoring.\n";
            return 1;
        }
        
        $match_pct = $best_token_match['url_tokens'] > 0
            ? $best_token_match['match'] * 100 / $best_token_match['url_tokens']
            : 0;

        if ($match_pct >= $this->_threshold_pct
        && $best_token_match['match'] > $this->_threshold_tokens) {
            print "\tBest match (matching tokens): ";
            printf("%d, %d, %d [%d pct]\n", $best_token_match['url_tokens'],
                $best_token_match['alt_tokens'], $best_token_match['match'],
                $match_pct);
            return 0;
        }

        else {
            print "\tThreshold values not passed. ";
            printf("%d, %d, %d [%d pct]\n", $best_token_match['url_tokens'],
                $best_token_match['alt_tokens'], $best_token_match['match'],
                $match_pct);
            print "\tCandidate was: " . $image_url . "\n";
            return 1;
        }
    }
    
    // Check for image attributes criteria.
    private function _size_criteria(
        $image_properties,
        $same_image_size_counter) {
    
        // image_properties[0]: image width
        // image_properties[1]: image height
    
        if ($same_image_size_counter == 0 ||
        $same_image_size_counter > 5) {
                print "\tMultiple or zero images of same size found ["
                    . $same_image_size_counter . "]. "
                    . "Possibly category page, ignoring.\n";
            return 1;
        }
        
        if ($image_properties[0] < $this->_threshold_dimension_px
        || $image_properties[1] < $this->_threshold_dimension_px) {
            print "\tThreshold values not passed. Image dimensions: ";
            printf("%d px, %d px\n", 
                $image_properties[0], $image_properties[1]);
            return 1;
        }
        
        elseif ($image_properties[0] > 1.51 * $image_properties[1]) {
            print "\tImage width much bigger than height ";
            printf("[%d px, %d px, ratio = %.1d pct]. ",
                $image_properties[0], $image_properties[1], 
                $image_properties[0] / $image_properties[1] * 100);
            print "Possible logo, ignoring.\n";
            return 1;
        }
        
        else {
            print "\tBest match (by size):   ";
            print $image_properties[0] * $image_properties[1] . " bytes\n";
            return 0;
        }
    
    }
       
    // Get price in the proximity of the best image.
    private function _get_price($node) {
        $price = null;
        $depth = 0;
        while (isset($node->parentNode)) {
            $node = $node->parentNode;
            /*$this->_common->writeit(2, "Price search in parent node ["
                . str_replace(array('\n', '\r'), ' ', 
                    substr($node->textContent, 0, 40))
                . "]");*/
            $price_arr = $this->_search_price($node);
            if (isset($price_arr)) {
                $this->_currency = $price_arr[0];
                $this->_price = $price_arr[1];
                print "\tProduct price: " . $this->_currency
                    . $this->_price . "\n";
                break;
            }
           
            $depth--;
        }
    
    }
    
    // Recursive helper function that searches for price match in
    // current DOM node and its children nodes.
    private function _search_price($node) {
    
        if (is_null($node)) {
            $this->_common->writeit(1, "Cannot search for price, "
                . "image node is NULL (image not found).\n");
            return NULL;
        }
    
        foreach ($node->childNodes as $child) {
            if (isset($child->textContent)) {
                $text = $child->textContent;
                /*$this->_common->writeit(2, "Price search in child node ["
                    . str_replace(array('\n', '\r'), ' ', substr($text, 0, 40))
                    . "]");*/

                foreach ($this->_currencies as $curr) {
                    if (preg_match("/($curr)\s*(\d[\d\.,]+)/", 
                    $text, $matches)) {
                        return array($matches[1], $matches[2]);
                    }
                }
            }
            
            if (isset($child->childNodes)) {
                $price_arr = $this->_search_price($child);
                if (isset($price_arr)) {
                    return $price_arr;
                }
            }
        }
        
        return NULL;
    }
    
    
    // Saves the original image and the thumbnail image locally.
    // Based on:
    // http://salman-w.blogspot.com/2008/10/ \
    //   resize-images-using-phpgd-library.html
    public function save_image_locally() {
    
        if (! isset($this->_image_url)) {
            return 0;
        }
        
        $corr_image_link = $this->_correct_image_link($this->_image_url);
        
        $image_path = $this->_build_path($this->_image_url, 0);
        if (! isset($image_path)) {
            return 0;
        }
        $thumbnail_image_path = $this->_build_path($this->_image_url, 1);
        if (! isset($thumbnail_image_path)) {
            return 0;
        }

        
        // Get image properties.
        list($source_image_width, $source_image_height, $source_image_type) = 
            getimagesize($corr_image_link);
            
        switch ($source_image_type) {
        
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($corr_image_link);
                break;
                
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($corr_image_link);
                break;
                
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($corr_image_link);
                break;
            
            default:
                $source_gd_image = false;
                $this->_common->writeit(1, "Image format not recognized.");
        }
        
        if ($source_gd_image === false) {
            return 0;
        }
        
        
        // Get original image ratio.
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = 
            $this->_thumb_max_width / $this->_thumb_max_height;
        
        // Resize sides based on the aspect ratio.
        if ($source_image_width <= $this->_thumb_max_width
        && $source_image_height <= $this->_thumb_max_height) {
        
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } 
        
        elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        
            $thumbnail_image_width = 
                (int) ($this->_thumb_max_height * $source_aspect_ratio);
            $thumbnail_image_height = $this->_thumb_max_height;
            
        } 
        
        else {
        
            $thumbnail_image_width = $this->_thumb_max_width;
            $thumbnail_image_height = 
                (int) ($this->_thumb_max_width / $source_aspect_ratio);
        }
        
        // Prepare and save original image.
        $gd_image = imagecreatetruecolor(
            $source_image_width, $source_image_height);
        imagecopy($gd_image, $source_gd_image, 0, 0, 0, 0, 
            $source_image_width, $source_image_height);
        imagejpeg($gd_image, $image_path, $this->_img_quality);
        
        // Prepare and save thumbnail image.
        $thumbnail_gd_image = imagecreatetruecolor(
            $thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, 
            $thumbnail_image_width, $thumbnail_image_height, 
            $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 
            $this->_thumb_quality);
        
        // Delete images from memory.
        imagedestroy($source_gd_image);
        imagedestroy($gd_image);
        imagedestroy($thumbnail_gd_image);
        
        // Save locations
        $this->_img_path = $image_path;
        $this->_thumb_path = $thumbnail_image_path;
    
        return 1;
    }
    
    // Creates a destination location for a given image.
    private function _build_path($url, $mode) {
    
        // Check if base dir exists.
        if (@is_dir($this->_img_base) === FALSE) {
            fwrite(STDERR, "ERROR: Not a directory: " . $this->_img_base);
            return null;
        }
        
        // If URL not set, return.
        if ($url == "") {
            $this->_common->writeit(2, "Image not set.");
            return null;
        }
        
        // Build image filename.
        $suffix = "";
        if ($mode == 1) {
            $suffix = "_t";
        }
        $filename = md5($url) . $suffix . ".jpg";
        
        // First subdir.
        $url_tokens = parse_url($url);
        $subdir_1 = $url_tokens['host'];
        $dir_1 = $this->_img_base . '/' . $subdir_1;
        if (@is_dir($dir_1) === FALSE) {
            $status = (mkdir($dir_1, 0755));
            if ($status === FALSE) {
                fwrite(STDERR, "ERROR: Failed to create: " . $dir_1);
                return null;
            }
        }
        
        // Second subdir (max 1000 subdirectories).
        $subdir_2 = substr($filename, 0, 3);
        $dir_2 = $dir_1 . '/' . $subdir_2;
        if (@is_dir($dir_2) === FALSE) {
            $status = (mkdir($dir_2, 0755));
            if ($status === FALSE) {
                fwrite(STDERR, "ERROR: Failed to create: " . $dir_2);
                return null;
            }
        }
        
        $filepath = $dir_2 . '/' . $filename;
        $this->_common->writeit(2, "Image filepath: " . $filepath);
        return $filepath;
    }
    
    // Stores image data (product URL, image URL) to the database.
    public function store_image_and_price_data() {
        
        $md5page = md5($this->_url);
        
        if ($this->_mode == 'crawler') {
            $tbl_page_links = TBL_PHPCRAWLER_LINKS;
            $col_page_url_md5 = "url_md5";
        }
        elseif ($this->_mode == 'sitemap') {
            $tbl_page_links = TBL_PAGE_LINKS;
            $col_page_url_md5 = "page_url_md5";
        }
        
        try {
            $q = <<<QUERY
UPDATE $tbl_page_links SET
page_title = :page_title,
image_url = :image_url,
image_url_md5 = MD5(image_url),
image_alt = :image_alt,
image_mode = :image_mode,
image_r_location = :image_r_location,
image_s_location = :image_s_location,
image_width = :image_width,
image_height = :image_height,
price = :price,
currency = :currency
WHERE $col_page_url_md5 = :md5page
QUERY;
            $ins = $this->_dbh->handler->prepare($q);
            $ins->bindParam(":md5page", $md5page);
            $ins->bindParam(":page_title", $this->_page_title);
            $ins->bindParam(":image_url", $this->_image_url);
            $ins->bindParam(":image_alt", $this->_img_alt_text);
            $ins->bindParam(":image_mode", $this->_img_mode);
            $ins->bindParam(":image_r_location", $this->_img_path);
            $ins->bindParam(":image_s_location", $this->_thumb_path);
            $ins->bindParam(":image_width", $this->_img_properties[0]);
            $ins->bindParam(":image_height", $this->_img_properties[1]);
            $ins->bindParam(":price", $this->_price);
            $ins->bindParam(":currency", $this->_currency);
            $ins->execute();
        }
        catch (PDOException $e) {
            echo "Error saving image: " . $e->getMessage() . "("
                . $e->getCode() . ")\n";
            return 0;
        }
        
        return 1;
    }
}


?>