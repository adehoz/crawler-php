<?php

// Test price tag with a page entry from the database.
// Usage:
//    php test_price_tag.php  - $page_id needs to be set in the code below.
//    php test_price_tag.php -i <page_id>  - $page_id passed as argument.

require_once('config.inc.php');
require_once('common.inc.php');
require_once('product.class.php');

    $opts = getopt("i:");

    $mode = 'sitemap';
    $domain_filter = null;
    $page_id = 242176;
    
    if (isset($opts['i'])) {    
        $page_id = $opts['i'];
    }

    $product = new Product($mode);
    if (isset($product)) print "Created new Product\n";
    
    $product->debug(DEBUG_LEVEL);
    $product->set_db(DBCONN, DBUSER, DBPASS);
    print "DB connection set\n";
    
    $product->set_domain_filter($domain_filter);
    print "Domain filter set\n";
    
    $product->set_image_basedir(IMAGE_BASE_PATH);
    print "Image base directory set\n";
    
    $rv = $product->find_url_by_id($page_id);
    print "Page data obtained\n";
    
    $rv = $product->set_url_tokens();
    if (! $rv) {
        print "URL not set\n";
        exit(2);
    }
    
    $rv = $product->get_content();
    if (! $rv) {
        print "Failed to get URL\n";
        exit (3);
    }
    
    $product->get_page_title();
    print "Page title ok\n";
    
    $product->get_product_image_and_price();
    print "Test done\n";

?>