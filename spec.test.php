<?php

require_once('config.inc.php');
require_once('common.inc.php');
require_once('spec.class.php');

DEFINE('DEBUG_MODE',  2);
DEFINE('TESTDIR',     'test');
DEFINE('INSTANCE',    '01');

$content_file = TESTDIR . '/content' . INSTANCE . '.txt';
$content_url_file = TESTDIR . '/content_url.txt';
$content = file_get_contents($content_file);

// Get URL for this instance.
$url = null;
$urls = file_get_contents($content_url_file);
$urls_arr = explode("\n", $urls);
foreach ($urls_arr as $url_def) {
    $tokens = explode(" ", $url_def, 2);
    if ($tokens[0] == INSTANCE) {
        $url = $tokens[1];
    }
}
if (isset($url)) {
    print "PAGE URL: " . $url . "\n";
}
else {
    print "PAGE URL NOT DEFINED IN " . $content_url_file . "\n";
}

$pricetag_glob = TESTDIR . '/content' . INSTANCE . '_pricetag*.txt';
foreach(glob($pricetag_glob) as $ftag) {
    print "==================================================\n";
    print $content_file . "\n";
    print $ftag . "\n";
    print "--------------------------------------------------\n";
    $price_tag = file_get_contents($ftag);
    $spec = new Spec($url, $content, $price_tag, DEBUG_MODE);
    $price = $spec->get_price();
    print "PRICE: " . $price['value'] . " " . $price['currency'] . "\n";
    $spec->get_image();
}

?>