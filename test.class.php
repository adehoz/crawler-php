<?php 

/*
 * Class Test provides common framework for testing classifier.
 */

class Test {

    private $_dbh = null;
    private $_products = null;
    private $_content = null;
    private $_domain_info = null;

    public function __construct() {
    }
    
    public function free() {
        $this->_content = null;
        $this->_domain_info = null;
    }
    
    // Sets database parameters.
    public function set_db($dbconn, $dbuser, $dbpass) {
        $this->_dbh = new Database($dbconn, $dbuser, $dbpass);
    }
    
    // Gets all test information from the DB table into _products.
    public function get_products() {
        if (is_null($this->_products)) {
            $this->_products = $this->_get_rows("SELECT * FROM test_pages");
        }
        return $this->_products;
    }
    
    // Gets page content into _content.
    public function get_content($filename) {
        $filepath = 'test/' . $filename;
        if (is_file($filepath)) {
            $this->_content = file_get_contents($filepath);
        }
        return $this->_content;
    }
    
    public function get_domain_data($id) {
        if (is_null($this->_domain_info)) {
            $this->_domain_info = $this->_get_row(
                "SELECT price_tag, sale_price_tag, image_tag, image_callback"
                . " FROM domains WHERE id = $id");
        }
        return $this->_domain_info;
    }
    
    
    private function _get_rows($query) {
        $sth = $this->_dbh->handler->prepare($query);
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if (FALSE === $rows) {
            return NULL;
        }
        return $rows;
    }
    
    private function _get_row($query) {
        $sth = $this->_dbh->handler->prepare($query);
        $sth->execute();
        $row = $sth->fetch(PDO::FETCH_ASSOC);
        if (FALSE === $row) {
            return NULL;
        }
        return $row;
    }
}

?>