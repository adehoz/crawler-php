#!/usr/bin/env bash
# Deletes domain and associated sitemaps and pages.
# Usage: clear-domain.sh <domain_id> -confirm

if [ -z $1 ]; then
	echo Please provide domain_id.
	exit 1
fi

if [[ -z $2 || $2 != '-confirm' ]]; then
	echo "Enter -confirm as a second parameter."
	exit 2
fi

DOMAIN_ID=$1
echo "delete from pages where sitemap_id in (select id from sitemaps where domain_id = ${DOMAIN_ID}); delete from sitemaps where domain_id = ${DOMAIN_ID}; delete from domains where id = ${DOMAIN_ID};" | mysql -u elmaquina --password=veloc1dad fashionome
echo "Done."
exit 0
