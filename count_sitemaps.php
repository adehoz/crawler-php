<?php
$urls = file('urls_original.txt');
$count_s = 0;
$count_d = 0;
$done = array();

foreach ($urls as $url) {

    $components = parse_url($url);
    $domain_url = $components['host'];
    $sitemap = $components['scheme'] . '://' . $domain_url . '/sitemap.xml';
    print $sitemap . "\n";

    if (isset($done[$sitemap])) {
        print "Already visited $sitemap\n";
        continue;
    }
    $done[$sitemap] = 1;
    
    $headers = get_headers($url, 0);

    foreach ($headers as $header) {
        if (preg_match("/HTTP\/1.\d (\d+)/", $header, $matches)) {
            $response_code = $matches[1];
            if ($response_code == 200) $count_s++;
        }
    }
    $count_d++;

    print "Count domains / sites: $count_d / $count_s\n";
}

?>