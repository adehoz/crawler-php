<?php

// Based on the decision tree learning algorithm ID3 described in:
// http://cis.k.hosei.ac.jp/~rhuang/Miccl/AI-2/L10-src/DecisionTree2.pdf

class DecisionTree
{

    // Properties.
    private $_training_file = 'training.csv';   // default training file
    private $_delimiter = ',';                  // data delimiter in the file
    
    private $_dataset = array();                // training data set
    private $_attributes = array();             // data set attributes.
    private $_positive_outcome = 'p';           // positive value in the
                                                // training data set.
    private $_root_node_entropy = 0;
    
    // Constructor.
    public function __construct() {
    
    }
    
    public function load_training_set() {
        
        $set = array();
        
        $rows = array_map("chop", file($this->_training_file));
        if ($rows === FALSE) {
            fwrite(STDERR, "Cannot read training data from "
                . $this->_training_file . "\n");
            return $set;
        }

        $count = 0;
        foreach ($rows as $row) {
            $tokens = explode($this->_delimiter, $row);
            if ($count++ == 0) {
                $this->_attributes = $tokens;
                continue;
            }
            $this->_dataset[] = $tokens;
            $set[] = $tokens;
        }
        
        return $set;
    }
    
    public function get_attributes() {
        return $this->_attributes;
    }
    
    public function root_node_entropy() {
    
        $LOG_BASE = 2;
        
        $set_size = count($this->_dataset);
        
        $positive_set_size = 0.0;
        foreach ($this->_dataset as $entry) {
            $decision_col = count($entry) - 1;
            if ($entry[$decision_col] == $this->_positive_outcome) {
                $positive_set_size += 1.0;
            }
        }
        
        $negative_set_size = $set_size - $positive_set_size;
        
        $entropy = -1 * ($positive_set_size / $set_size) 
            * log($positive_set_size / $set_size, $LOG_BASE) 
            - ($negative_set_size / $set_size)
            * log($negative_set_size / $set_size, $LOG_BASE);
    
        //print "Entropy: " . $entropy . "\n";
        $this->_root_node_entropy = $entropy;
    }
    
    private function _entropy($set, $attr) {
    
        $LOG_BASE = 2;
        
        $positive_set_size = 0;
        $negative_set_size = 0;
        foreach ($set[$attr] as $outcome => $count) {
            if ($outcome == $this->_positive_outcome) {
                $positive_set_size += $count;
            }
            else {
                $negative_set_size += $count;
            }
        }
        
        // Entropy is zero if all set members belong to the same class.
        if ($positive_set_size == 0 || $negative_set_size == 0) {
            return 0;
        }
        
        $attr_set_size = $positive_set_size + $negative_set_size;
        
        $entropy = -1 * ($positive_set_size / $attr_set_size) 
            * log($positive_set_size / $attr_set_size, $LOG_BASE)
            - ($negative_set_size / $attr_set_size)
            * log($negative_set_size / $attr_set_size, $LOG_BASE);

        //print "calc entropy: $positive_set_size : $negative_set_size";
        //print "\tentropy: $entropy\n";

        return $entropy;
    }
    
    private function _gain($attribute_set) {

        $set_size = count($this->_dataset);
        
        $gain = $this->_root_node_entropy;
        //print "* GAIN: $gain\n";
        foreach ($attribute_set as $div => $attribute_div_val) {
        
            $attribute_set_size = 0;
            foreach ($attribute_div_val as $size) {
                $attribute_set_size += $size;
            }
        
            $attr_gain = ($attribute_set_size / $set_size)
                * $this->_entropy($attribute_set, $div);
            //print "CALCULATING ATTR GAIN FOR $div : $attribute_set_size"
            //    . " : $set_size\n";
            //print "\tcalculated: $attr_gain\n";
            $gain -= $attr_gain;
        }
        
        return $gain;
    }
    
    private function _build_set($set, $attributes) {
    
        $information = array();
        
        foreach ($set as $entry) {
        
            for ($i = 0; $i < count($entry) - 1; $i++) {
            
                if ($attributes[$i] == 'ignore') {
                    continue;
                }
            
                if (! isset(
                $information
                    [$attributes[$i]]
                    [$entry[$i]]
                    [$entry[count($entry) - 1]]
                    )
                ) {
                    $information
                        [$attributes[$i]]
                        [$entry[$i]]
                        [$entry[count($entry) - 1]] = 1;
                }
                
                else {
                    $information
                        [$attributes[$i]]
                        [$entry[$i]]
                        [$entry[count($entry) - 1]]++;
                }
            }
        }
        
        return $information;
    }
    
    private function _group_outcomes($set) {
    
        $outcomes = array();
        
        foreach ($set as $entry) {
        
            $outcome = $entry[count($entry) - 1];
            
            if (! isset($outcomes[$outcome])) {
                $outcomes[$outcome] = 1;
            }
            
            else {
                $outcomes[$outcome]++;
            }
        }
        
        return $outcomes;
    }
    
    private function _select_subset($set, $attribute, $value) {
    
        $subset = array();
        $index = array_search($attribute, $this->_attributes);
        
        foreach ($set as $row) {
            if ($row[$index] == $value) {
                $subset[] = $row;
            }
        }
    
        return $subset;
    }
    
    public function learn($set, $attributes) {
    
        $information = $this->_build_set($set, $attributes);
        $tree = array();
        
        $outcomes = $this->_group_outcomes($set);
        $tree['outcomes'] = $outcomes;
        
        // If all examples are positive or negative, return a single-node
        // tree with the corresponding label.
        if (count($outcomes) == 1) {
        
            $tree['nodes'][] = array_keys($outcomes)[0];
            return $tree;
        }
        
        // Calculate information gain for each attribute.
        // Get decision attribute.
        $gain = array();
        $set_size = count($set);

        $max_gain = 0.0;
        $decision_attribute = '';
        foreach ($information as $attribute => $attr_set) {
        
            $gain = $this->_gain($attr_set);
            if ($gain > $max_gain) {
                $max_gain = $gain;
                $decision_attribute = $attribute;
            }
            
            $tree['gains'][$attribute] = $gain;
        }
        
        foreach ($information[$decision_attribute] as $v => $occurences) {
        
            $tree['decision_attribute'] = $decision_attribute;
            $tree['nodes'][$decision_attribute . "." . $v] = array(
                'attribute' => $decision_attribute,
                'value'     => $v
            );
            
            $subset = $this->_select_subset($set, $decision_attribute, $v);
            
            if (empty($subset)) {
            
                $tree['nodes'][$decision_attribute . "." . $v][] =
                   array_search(max($outcomes), $outcomes); 
            }
            
            else {
                
                $attributes_s = array();
                foreach ($attributes as $a) {
                    if ($a == $decision_attribute) {
                        $a = 'ignore';
                    }
                    $attributes_s[] = $a;
                }
                
                $tree['nodes'][$decision_attribute . "." . $v] =
                    $this->learn($subset, $attributes_s);
            }
        }

        // This gets returned recursively, here be dragons.
        return $tree;
    }
    
    public function test($node, $data) {
   
        if (!isset($node['decision_attribute'])
        && count($node['nodes']) == 1) {
            return $node['nodes'][0];
        }
        
        $attribute = $node['decision_attribute'];
        $value = $data[$attribute];
        
        print "Attribute: $attribute\n";
        print "Value:     $value\n\n";
        
        if (! isset($node['nodes'][$attribute . '.' . $value])) {
            return NULL;
        }
        return $this->test($node['nodes'][$attribute . '.' . $value], $data);
    }
    
}

/* // This is an example run. The class is called from classifier.php
$dt = new DecisionTree();
$set = $dt->load_training_set();
$dt->root_node_entropy();
$tree = $dt->learn($set, $dt->get_attributes());
$outcome = $dt->test($tree, array(
    'outlook' => 'rain',
    'temperature' => 'hot',
    'humidity' => 'normal',
    'windy' => 'FALSE'
));

print "Outcome: $outcome\n";
*/
?>